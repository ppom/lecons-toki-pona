---
title: Leçon 13. Êtres vivants
date: 2022-11-13
in_search_index: true
extra:
  number: 13
---

# 13. Êtres vivants

## Vocabulaire

- **akesi** - reptile, batracien, animal non aimable
- **kala** - poisson, animal marin
- **kasi** - plante
- **moli** - mort; mourir, tuer
- **pipi** - insecte, araignée
- **soweli** - mammifère
- **waso** - oiseau

Les noms d'êtres vivants sont à peu près aussi vagues que le reste en
toki pona. Chaque nom couvre une grande catégorie à la frontière parfois
assez floue.

## *akesi*

*akesi* est une catégorie qui recouvre les reptiles, les batraciens, et
plus généralement les animaux terrestres effrayants, visqueux, ou
monstrueux : serpent, lézard, crocodile, tortue, etc. Un dinosaure ou un
monstre quelconque (troll, etc.) est également un *akesi*. Le grand
méchant loup des contes de fées, bien qu'étant un mammifère, sera sans
doute traduit par *akesi*.

Pour être plus spécifique on peut essayer de préciser avec des adjectifs
ou des périphrases. Par exemple *akesi pi telo moli* désignera un
*akesi* venimeux.

## *kala*

*kala* désigne n'importe quel animal marin.

-   **{{_(_="kala")}} {{_(_="lete")}} {{_(_="li")}} {{_(_="ike")}} {{_(_="tawa")}} {{_(_="mi")}}.** - Je n'aime pas le poisson cru.
-   **{{_(_="jan")}} Ulise {{_(_="li")}} {{_(_="wile")}} {{_(_="kute")}} {{_(_="e")}} {{_(_="jan")}} {{_(_="kala")}}.** - Ulysse voulait entendre les
    sirènes.

## *kasi*

*kasi* désigne toutes les plantes, depuis les grands arbres jusqu'au
moindre lichen. Voici quelques expressions :

-   **{{_(_="kasi")}} {{_(_="kule")}}** - fleur
-   **{{_(_="kasi")}} {{_(_="suli")}}** - arbre
-   **{{_(_="kasi")}} {{_(_="lili")}}** - herbes, broussailles, etc.
-   **{{_(_="lipu")}} {{_(_="kasi")}}** - feuille
-   **{{_(_="ma")}} {{_(_="kasi")}}** - forêt, jardin

Il existe certaine plante, illégale dans la plupart des pays, dont il
paraît qu'elle aurait grandement inspiré le toki pona. Utilisée dans un
but récréatif, cela s'appelle *{{_(_="kasi")}} {{_(_="nasa")}}*. Utilisé pour la méditation
(?) cela s'appelle *{{_(_="kasi")}} {{_(_="sona")}}*.

## *soweli*

*soweli* désigne généralement l'ensemble des mammifères et des animaux
terrestres « aimables ». Le mot désigne également la viande de *soweli*.

-   **{{_(_="soweli")}} {{_(_="ni")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="moku")}}.** - Ce steak est délicieux.

## *waso*

*waso* désigne tous les oiseaux et animaux volant.

-   **{{_(_="mi")}} {{_(_="wile")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="waso")}}.** - Je veux manger du poulet.

## *moli*

*moli* s'emploie comme adjectif et comme verbe transitif. Pour dire que
quelque chose est en train de mourir, on utilise la tournure avec *kama*
pour indiquer le changement d'état.

-   **{{_(_="pipi")}} {{_(_="li")}} {{_(_="moli")}}.** - L'insecte est mort.
-   **{{_(_="soweli")}} {{_(_="li")}} {{_(_="kama")}} {{_(_="moli")}}.** - Le cheval est en train de mourir.
-   **{{_(_="jan")}} {{_(_="li")}} {{_(_="moli")}} {{_(_="e")}} {{_(_="waso")}}.** - La personne a tué l'oiseau.

## Exercices

### Traduisez ces phrases du français au toki pona.

Je veux un cochon d'Inde.<details>{{_(_="mi")}} {{_(_="wile")}} {{_(_="e")}} {{_(_="soweli")}} {{_(_="lili")}}.</details>
Ah! Le dinosaure veut me manger!<details>{{_(_="a")}}! {{_(_="akesi")}} {{_(_="li")}} {{_(_="wile")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="mi")}}!</details>
La vache meugle.<details>{{_(_="soweli")}} {{_(_="li")}} {{_(_="mu")}}.</details>
Les oieaux volent en l'air.<details>{{_(_="waso")}} {{_(_="li")}} {{_(_="tawa")}} {{_(_="lon")}} {{_(_="kon")}}.</details>
Mangeons du poisson.<details>{{_(_="mi")}} {{_(_="mute")}} {{_(_="o")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="kala")}}.</details>
Les fleurs sont belles.<details>{{_(_="kasi")}} {{_(_="kule")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="lukin")}}.</details>

### Et maintenant essayez de traduire du toki pona en français.

mama ona li kepeken e kasi nasa.<details>Son père fume du haschich.</details>
akesi li pana e telo moli.<details>Le serpent crache son venin.</details>
pipi li moku lili e lipu kasi.<details>L'insecte grignote la feuille.</details>
soweli mi li kama moli.<details>Mon chien meurt.</details>
kasi li pona tawa mi.<details>J'aime les plantes.</details>
mi lon ma kasi.<details>Je suis dans la forêt.</details>
