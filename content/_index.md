+++
+++

# Leçons de toki pona

## Introduction

Le toki pona est une langue artificielle inventée par Sonja Elen Kisa et
publiée en 2001. « toki pona » signifie « langue bonne, langue du
bien ». La langue se base sur la philosophie du Dao, et vise avant tout
la simplicité de la phonologie, de la grammaire, et du vocabulaire, ce
qui la rend facile à apprendre. Le vocabulaire ne comporte que 118 mots
(et pas un de plus : le dictionnaire officiel est intentionnellement
figé) et pourtant on est surpris de la relative expressivité qu'il
autorise.

Le toki pona n'est pas parlé par beaucoup de monde. Il existe cependant
un groupe de discussion sur Yahoo, un forum IRC, quelques pages web
contenant une littérature pour l'instant limitée (oeuvres originales et
traductions).

J'ai commencé à écrire ces leçons en français comme une traduction
littérale des excellentes leçons en anglais de jan Pije, qui ont fait
autorité pendant ces dernières années. Au cours de traduction, j'ai
pris connaissance de la version en esperanto que Jim Henry avait juste
commencé à publier (un peu en aveugle à vrai dire car je n'ai pas
appris l'esperanto). Ces leçons sont donc une traduction et une
adaptation à la fois des versions de jan Pije et de Jim Henry. Je leur
addresse ici mes remerciements.

## Sommaire

(les numéros de chapitre ne coïncident ni avec la numérotation de jan
Pije, ni avec celle de Jim Henry.)

-   [1. Prononciation et ortographe](lecon1)
-   [2. Premières phrases](lecon2)
-   [3. Complément d'objet direct](lecon3)
-   [4. Adjectifs, adverbes et noms composés](lecon4)
-   [5. Prépositions *lon*, *kepeken*, et *tawa*](lecon5)
-   [6. Autres prépositions](lecon6)
-   [7. Négation; questions simples](lecon7)
-   [8. Genre; mots non officiels; impératif](lecon8)
-   [9. Questions avec *seme*](lecon9)
-   [10. *pi*](lecon10)
-   [11. Conjonctions; température](lecon11)
-   [12. Couleurs](lecon12)
-   [13. Êtres vivants](lecon13)
-   [14. Le corps](lecon14)
-   [15. Nombres](lecon15)
-   [16. *la*](lecon16)
-   [17. Conclusion](lecon17)

## À propos

Ce document est une libre modification de [lvogel.free.fr](http://lvogel.free.fr/tokipona).

J'ai fait des modifications du fond et de la forme.

Contactez-moi en envoyant un mail à destination de : tokipona "*arobase*" ppom.me

Les fichiers qui permettent l'existence de ce site sont [ici](https://framagit.org/ppom/lecons-toki-pona).

[www.tokipona.org](http://www.tokipona.org) - site officiel du toki
    pona.
