---
title: Leçon 10. *pi*
date: 2022-11-10
in_search_index: true
extra:
  number: 10
---

# 10. *pi*

## Vocabulaire

- **pi** - particule
- **kalama** - son, bruit
- **kute** - écouter, entendre
- **kulupu** - groupe, communauté, société
- **nasin** - voie, chemin, méthode, doctrine

## *pi*

On considère souvent que *pi* est une des plus grande difficultés
grammaticales du toki pona. Souvent la confusion vient du fait que l'on
cherche à raccorder *pi* à une catégorie grammaticale existante en
français (article, préposition, etc.) alors que *pi* n'a en réalité
aucun équivalent réel en français.

**Définition :** la particule *pi* est utilisée pour regrouper
des mots composés lorsque ces mots composés sont utilisés comme
qualificatifs d'un nom.

On se souvient qu'on a vu à la leçon 3 que les qualificatifs sont
ajoutés un par un, et qu'ils précisent tous la signification du nom
qu'ils qualifient. Comparons ces phrases (souvenons-nous : *tomo telo*
= salle d'eau, *telo nasa* = alcool).

-   **{{_(_="mi")}} {{_(_="tawa")}} {{_(_="tomo")}} {{_(_="telo")}} {{_(_="nasa")}}.** - je vais dans la salle de bains
    bizarre.
-   **{{_(_="mi")}} {{_(_="tawa")}} {{_(_="tomo")}} {{_(_="pi")}} {{_(_="telo")}} {{_(_="nasa")}}.** - Je vais au bar.

Dans la première phrase, *telo* qualifie *tomo*, puis *nasa* qualifie
l'ensemble *telo tomo*. L'usage de *pi* permet de remettre le curseur
à zéro et de faire en sorte que les qualificatifs restants qualifient le
mot suivant *pi* au lieu du mot initial. Ainsi, dans la deuxième phrase
*nasa* est le qualificatif de *telo*, et c'est l'ensemble *telo nasa*
qui qualifie *tomo*.

Autres exemples :

-   **{{_(_="jan")}} {{_(_="pi")}} {{_(_="ma")}} {{_(_="tomo")}}** - citadin
-   **{{_(_="kulupu")}} {{_(_="pi")}} {{_(_="toki")}} {{_(_="pona")}}** - la communauté du toki pona
-   **{{_(_="nasin")}} {{_(_="pi")}} {{_(_="toki")}} {{_(_="pona")}}** - la méthode du toki pona
-   **{{_(_="jan")}} {{_(_="lawa")}} {{_(_="pi")}} {{_(_="jan")}} {{_(_="utala")}}** - officier
-   **{{_(_="jan")}} {{_(_="lawa")}} {{_(_="pi")}} {{_(_="tomo")}} {{_(_="tawa")}} {{_(_="kon")}}** - pilote d'avion
-   **{{_(_="jan")}} {{_(_="pi")}} {{_(_="nasin")}} {{_(_="sewi")}} Kolisu** - chrétien

*pi* permet également d'utiliser les adjectifs composés comme *pona
lukin* :

-   **{{_(_="mi")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="jan")}} {{_(_="pona")}} {{_(_="lukin")}}.** - je regarde l'ami visuel.
-   **{{_(_="mi")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="jan")}} {{_(_="pi")}} {{_(_="pona")}} {{_(_="lukin")}}.** - Je regarde la belle personne.

De même que l'un des usages des qualificatifs est d'indiquer la notion
de possession, cela est vrai également des qualificatifs composés avec
*pi*.

-   **{{_(_="kili")}} {{_(_="mi")}}** - mon fruit
-   **{{_(_="kili")}} {{_(_="pi")}} {{_(_="jan")}} Pita** - Le fruit de Pierre
-   **{{_(_="ma")}} {{_(_="ona")}}** - sa terre
-   **{{_(_="ma")}} {{_(_="pi")}} {{_(_="jan")}} {{_(_="pali")}}** - la terre du travailleur
-   **{{_(_="len")}} {{_(_="jan")}}** - la veste de quelqu'un
-   **{{_(_="len")}} {{_(_="pi")}} {{_(_="jan")}} {{_(_="lawa")}}** - la veste du chef
-   **{{_(_="o")}} {{_(_="kama")}} {{_(_="tawa")}} {{_(_="tomo")}} {{_(_="mi")}}!** - Viens chez moi!
-   **{{_(_="jan")}} Ken {{_(_="li")}} {{_(_="tawa")}} {{_(_="tomo")}} {{_(_="pi")}} {{_(_="jan")}} Lisa.** - Ken va dans la maison de
    Lisa.

Exemple avec les pronoms composés (*mi mute*, etc) :

-   **{{_(_="kili")}} {{_(_="mi")}} {{_(_="mute")}}** - mes nombreux fruits (*mute* modifie *kili mi*)
-   **{{_(_="kili")}} {{_(_="pi")}} {{_(_="mi")}} {{_(_="mute")}}** - notre fruit, nos fruits

Encore un exemple avec *ala* :

-   **{{_(_="jan")}} {{_(_="wawa")}}** - personne puissante
-   **{{_(_="jan")}} {{_(_="wawa")}} {{_(_="ala")}}** - aucune personne puissante
-   **{{_(_="jan")}} {{_(_="pi")}} {{_(_="wawa")}} {{_(_="ala")}}** - personne faible

### Qualificatifs supplémentaires

Nous avons vu que les qualificatifs supplémentaires sont habituellement
ajoutés à la fin. Ce n'est néanmoins pas possible avec *pi* si le
qualificatif supplémentaire se rapporte au mot de tête (et non au mot
composé suivant *pi*) :

-   **{{_(_="len")}} {{_(_="pi")}} {{_(_="jan")}} {{_(_="lawa")}} {{_(_="lili")}}** - le vêtement du petit chef
-   **{{_(_="len")}} {{_(_="lili")}} {{_(_="pi")}} {{_(_="jan")}} {{_(_="lawa")}}** - le petit vêtement du chef

C'est également le cas avec les pronoms, même si d'habitude on les
ajoute à la fin :

-   **{{_(_="tomo")}} {{_(_="telo")}} {{_(_="seme")}}** - quelle salle de bain?
-   **{{_(_="tomo")}} {{_(_="seme")}} {{_(_="pi")}} {{_(_="telo")}} {{_(_="nasa")}}** - quel bar?
-   **{{_(_="jan")}} {{_(_="lawa")}} {{_(_="ni")}} {{_(_="pi")}} {{_(_="tomo")}} {{_(_="tawa")}}** - ce pilote
-   **{{_(_="jan")}} {{_(_="lawa")}} {{_(_="pi")}} {{_(_="tomo")}} {{_(_="tawa")}} {{_(_="ni")}}** - le pilote de ce véhicule

### Erreurs courantes

*pi* sert à regrouper ensemble des qualificatifs. On ne doit donc pas
l'utiliser entre un mot et son unique qualificatif. On ne dit pas
**{{_(_="jan")}} {{_(_="pi")}} {{_(_="wawa")}}* mais *{{_(_="jan")}} {{_(_="wawa")}}*.

Par ailleurs, même si les constructions avec *pi* s'expriment souvent
en français utilisant le mot « de », il ne faut pas traduire
systématiquement « de » par *pi* :

-   **{{_(_="mi")}} {{_(_="kama")}} {{_(_="tan")}} {{_(_="ma")}} Mewika.** - Je viens des États-Unis.
    (et non **{{_(_="mi")}} {{_(_="tawa")}} {{_(_="pi")}}...*)
-   **{{_(_="ona")}} {{_(_="li")}} {{_(_="toki")}} {{_(_="e")}} {{_(_="sina")}}.** - il parle de toi
    (et non **{{_(_="ona")}} {{_(_="li")}} {{_(_="toki")}} {{_(_="pi")}}...*)

## Mots divers

### *kalama*, *kute*

*kalama* signifie « son », « bruit ». *kute* est un verbe signifiant
« écouter », « entendre ».

-   **{{_(_="kalama")}} {{_(_="ni")}} {{_(_="li")}} {{_(_="seme")}}?** - Quel est ce bruit?
-   **{{_(_="mi")}} {{_(_="ken")}} {{_(_="ala")}} {{_(_="kute")}} {{_(_="e")}} {{_(_="toki")}} {{_(_="sina")}}** - Je n'arrive pas à entendre ce que
    {{_(_="tu")}} dis.
-   **{{_(_="kalama")}} {{_(_="musi")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="mi")}}.** - J'aime la musique.
    [*{{_(_="kalama")}} {{_(_="musi")}}* (bruit récréatif) = musique]
-   **{{_(_="kalama")}} {{_(_="musi")}} {{_(_="pi")}} {{_(_="jan")}} Petowen {{_(_="li")}} {{_(_="pona")}} {{_(_="kute")}}.** - La musique de
    Beetoven est agréable à entendre.

### *nasin*

*nasin* signifie « chemin » mais aussi « méthode ». Ce dernier sens
permet de traduire le français « comme », « comment ».

-   **{{_(_="sina")}} {{_(_="pali")}} {{_(_="e")}} {{_(_="ni")}} {{_(_="kepeken")}} {{_(_="nasin")}} {{_(_="seme")}}?** - Comment fais-tu cela?
-   **{{_(_="o")}} {{_(_="toki")}} {{_(_="kepeken")}} {{_(_="nasin")}} {{_(_="ni")}}!** - parle de cette façon!

### *e* et *lon* pour dire « à propos »

Il n'y a pas de consensus parmi les tokiponistes sur la façon de
traduire « parler au sujet de ». Certains utilisent un complément
d'objet direct, donnant à *toki* transitif le sens de « parler de ».
D'autres recommandent d'utiliser la préposition généraliste *lon*,
mais ça pose un problème car du coup il y a risque de confusion avec par
exemple un complément de lieu.

-   **{{_(_="mi")}} {{_(_="toki")}} {{_(_="lon")}} {{_(_="jan")}}.** - Je parle des hommes.
-   **{{_(_="ona")}} {{_(_="li")}} {{_(_="toki")}} {{_(_="e")}} {{_(_="sona")}} {{_(_="toki")}} {{_(_="tawa")}} {{_(_="jan")}} {{_(_="lili")}} {{_(_="lon")}} {{_(_="tomo")}} {{_(_="sona")}}** - Il parle
    de linguistique à l'enfant dans l'école.
-   **{{_(_="sina")}} {{_(_="toki")}} {{_(_="ala")}} {{_(_="toki")}} {{_(_="lon")}} {{_(_="tomo")}} {{_(_="sona")}}?** - Parles-tu de l'école?
    *ou :* parles-tu dans l'école?
-   **{{_(_="mi")}} {{_(_="toki")}} {{_(_="lon")}} {{_(_="tomo")}} {{_(_="sona")}} {{_(_="lon")}} {{_(_="tomo")}} {{_(_="mi")}}.** - Je parle de l'école dans
    ma maison. *ou :* Je parle de ma maison à l'école.

Personnellement j'utilise et je recommande *e*. C'est le plus logique
et le moins ambigu. Mais jan Sonja (qui a créé le toki pona) recommande
*lon*, au motif qu'il y a rarement ambiguïté. En final il faut être
capable de comprendre les deux tournures.

-   **{{_(_="ona")}} {{_(_="li")}} {{_(_="toki")}} {{_(_="tawa")}} {{_(_="pipi")}}.** - Il parle à l'insecte.
-   **{{_(_="jan")}} {{_(_="lili")}} {{_(_="li")}} {{_(_="toki")}} {{_(_="e")}} {{_(_="ni")}}: "{{_(_="mi")}} {{_(_="wile")}} {{_(_="e")}} {{_(_="suwi")}}."** - L'enfant dit
    ceci : « je veux un bonbon. »
-   **{{_(_="jan")}} {{_(_="sona")}} {{_(_="toki")}} {{_(_="li")}} {{_(_="toki")}} {{_(_="e")}} {{_(_="toki")}} Lasina.** - Le linguiste discute au
    sujet du latin.
-   **{{_(_="jan")}} {{_(_="sona")}} {{_(_="toki")}} {{_(_="li")}} {{_(_="toki")}} {{_(_="kepeken")}} {{_(_="toki")}} Lasina.** - Le linguiste parle
    latin.

## Exercices

### Traduisez ces phrases du français au toki pona.

L'enfant de Keli s'amuse.<details>{{_(_="jan")}} {{_(_="lili")}} {{_(_="pi")}} {{_(_="jan")}} Keli {{_(_="li")}} {{_(_="musi")}}.</details>
Je suis tokiponiste.<details>{{_(_="mi")}} {{_(_="jan")}} {{_(_="pi")}} {{_(_="toki")}} {{_(_="pona")}}.</details>
C'est un ami musicien.<details>{{_(_="ona")}} {{_(_="li")}} {{_(_="jan")}} {{_(_="pona")}} {{_(_="pi")}} {{_(_="kalama")}} {{_(_="musi")}}.</details>
Le pilote d'avion mange.<details>{{_(_="jan")}} {{_(_="lawa")}} {{_(_="pi")}} {{_(_="tomo")}} {{_(_="tawa")}} {{_(_="telo")}} {{_(_="li")}} {{_(_="moku")}}.</details>
Miaou.<details>{{_(_="mu")}}.</details>
Qui est gros dans ce groupe?<details>{{_(_="jan")}} {{_(_="seme")}} {{_(_="pi")}} {{_(_="kulupu")}} {{_(_="ni")}} {{_(_="li")}} {{_(_="suli")}}?</details>
Nos maisons sont abimées.<details>{{_(_="tomo")}} {{_(_="pi")}} {{_(_="mi")}} {{_(_="mute")}} {{_(_="li")}} {{_(_="pakala")}}.</details>
Comment fait-on cela?<details>{{_(_="jan")}} {{_(_="li")}} {{_(_="pali")}} {{_(_="e")}} {{_(_="ni")}} {{_(_="kepeken")}} {{_(_="nasin")}} {{_(_="seme")}}?</details>

### Et maintenant essayez de traduire du toki pona en français.

kili pi jan Linta li ike.<details>L'enfant de Linda est méchant.</details>
len pi jan Susana li jaki.<details>Le vêtement de Susanne est sale.</details>
mi sona ala e nimi pi ona mute.<details>Je ne connais pas leurs noms.</details>
mi wile ala toki lon kalama musi.<details>Je ne veux pas parler de musique.</details>
mi wile toki e meli.<details>Je veux parler des femmes.</details>
sina pakala e ilo telo len kepeken nasin seme?<details>Comment as-tu cassé le lave linge?</details>
jan Wasintan li jan lawa pona pi ma Mewika.<details>Washington fut un bon dirigeant des États-unis.</details>
wile pi jan ike li pakala e ijo.<details>La volonté des méchants ruine les choses.</details>
