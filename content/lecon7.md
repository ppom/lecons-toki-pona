---
title: Leçon 7. Négation; questions simples
date: 2022-11-07
in_search_index: true
extra:
  number: 7
---

# 7. Négation; questions simples

## Vocabulaire

- **ala** - non, aucun; rien
- **ale**, **ali** - tout, tous
- **ken** - pouvoir (verbe), être capable de; possibilité
- **lape** - dormir; repos
- **musi** - amuser, jouer; jeu, divertissement
- **pali** - faire, fabriquer, travailler; acte, activité, travail
- **sona** - connaître, savoir; connaissance, sagesse
- **wawa** - fort, intense; énergie, puissance

## Négation

En toki pona on exprime la négation et ajoutant le mot *ala* après le
verbe.

- **{{_(_="mi")}} {{_(_="lape")}} {{_(_="ala")}}.** - Je ne dors pas.
- **{{_(_="mi")}} {{_(_="musi")}} {{_(_="ala")}}.** - Je ne m'amuse pas.
- **{{_(_="mi")}} {{_(_="wawa")}} {{_(_="ala")}}.** - Je ne suis pas fort.
- **{{_(_="mi")}} {{_(_="wile")}} {{_(_="ala")}} {{_(_="tawa")}} {{_(_="musi")}}.** - Je ne veux pas danser.
 *tawa musi* signifie mot à mot quelque chose comme « bouger de
 manière amusante ». Cela signifie « danser ».

Bien entendu les phrases avec *ala* peuvent souvent être traduites de
nombreuses façons, puisqu'il y a souvent en français deux mots pour une
chose et son contraire. Par exemple, « *{{_(_="mi")}} {{_(_="musi")}} {{_(_="ala")}}* » peut aussi se
traduire par « je m'ennuie »; « *{{_(_="mi")}} {{_(_="wawa")}} {{_(_="ala")}}* » par « je suis faible ».

*ala* peut aussi être utilisé en complément du nom comme un adjectif.

- **{{_(_="jan")}} {{_(_="ala")}} {{_(_="li")}} {{_(_="toki")}}.** - Personne ne parle
- **{{_(_="toki")}} {{_(_="ala")}} {{_(_="li")}} {{_(_="ken")}} {{_(_="toki")}} {{_(_="e")}} {{_(_="ijo")}}.** - L'absence de mots peut exprimer
 quelque chose

*ala* peut même être utilisé comme un pronom, et dans ce cas il signifie
« rien » (on évite donc de dire *{{_(_="ijo")}} {{_(_="ala")}}* qui signifie la même chose
mais est moins élégant).

- **{{_(_="ala")}} {{_(_="li")}} {{_(_="jaki")}}.** - Rien n'est sale

## *ale*/*ali*

*ale* et *ali* sont en fait le même mot avec deux prononciation
différentes. Initialement il y avait juste *ale* mais il se trouve que
certains prononcent *ala* et *ale* presque de la même façon, et c'est
pourquoi *ali* a été introduit. Les deux variantes *ale* et *ali*
co-existent pour l'instant, avec peut-être une légère préférence pour
*ali* qui est la variante retenue pour ces leçons.

*ale*/*ali*, comme adjectif et pronom, s'emploie de la même façon
qu'*ala*, et signifie « tout, tous. »

- **{{_(_="jan")}} {{_(_="ali")}} {{_(_="li")}} {{_(_="wile")}} {{_(_="tawa")}}.** - Tout le monde veut voyager.
- **{{_(_="ma")}} {{_(_="ali")}} {{_(_="li")}} {{_(_="pona")}}.** - Tous les pays sont bons.
- **{{_(_="ali")}} {{_(_="li")}} {{_(_="pona")}}.** - Tout est bien (proverbe de toki pona).

## Questions simples

Pour poser une question à laquelle on peut répondre par oui ou par non,
on utilise une structure particulière. Voyons sur un exemple :

- **{{_(_="sina")}} {{_(_="pona")}} {{_(_="ala")}}.** - Tu n'es pas bien.
- **{{_(_="sina")}} {{_(_="pona")}}.** - Tu es bien.
- **{{_(_="sina")}} {{_(_="pona")}} {{_(_="ala")}} {{_(_="pona")}}?** - Es-tu bien? (littéralement: « toi pas-bien
 bien? »)

Pour poser une question simple comme celle-là, il faut dire le verbe,
puis *ala*, puis répéter le verbe et enfin finir la phrase. Voici
d'autres exemples :

- **{{_(_="suno")}} {{_(_="li")}} {{_(_="suli")}} {{_(_="ala")}} {{_(_="suli")}}?** - Est-ce que le soleil est grand?
- **{{_(_="len")}} {{_(_="sina")}} {{_(_="li")}} {{_(_="telo")}} {{_(_="ala")}} {{_(_="telo")}}?** - Tes vêtements sont-ils mouillés?
- **{{_(_="tomo")}} {{_(_="tawa")}} {{_(_="sina")}} {{_(_="li")}} {{_(_="pakala")}} {{_(_="ala")}} {{_(_="pakala")}}?** - Ta voiture est-elle
 cassée?
- **{{_(_="sina")}} {{_(_="ken")}} {{_(_="ala")}} {{_(_="ken")}} {{_(_="lape")}}?** - peux-tu dormir?
- **{{_(_="ona")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="ala")}} {{_(_="lon")}} {{_(_="tomo")}}?** - Est-il dans la maison?
- **{{_(_="ona")}} {{_(_="li")}} {{_(_="tawa")}} {{_(_="ala")}} {{_(_="tawa")}} {{_(_="ma")}} {{_(_="ike")}}?** - Est-il allé à la mauvaise terre?
- **{{_(_="sina")}} {{_(_="pana")}} {{_(_="ala")}} {{_(_="pana")}} {{_(_="e")}} {{_(_="moku")}} {{_(_="tawa")}} {{_(_="jan")}} {{_(_="lili")}}?** - Donnes-tu à manger à
 l'enfant?
- **{{_(_="pipi")}} {{_(_="li")}} {{_(_="moku")}} {{_(_="ala")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="kili")}}?** - Est-ce que les insectes mangent
 les fruits?

## Réponse

Pour répondre à une telle question on répète le verbe suivi ou non
d'*ala* selon que la réponse est « oui » ou « non » :

- **{{_(_="sina")}} {{_(_="wile")}} {{_(_="ala")}} {{_(_="wile")}} {{_(_="moku")}}?** - Veux-tu manger?
 - **{{_(_="wile")}}** - Oui.
 - **{{_(_="wile")}} {{_(_="ala")}}** - Non.
- **{{_(_="sina")}} {{_(_="lukin")}} {{_(_="ala")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="kiwen")}}?** - Vois-tu le rocher?
 - **{{_(_="lukin")}}** - Oui.
 - **{{_(_="lukin")}} {{_(_="ala")}}** - Non.
- **{{_(_="sina")}} {{_(_="sona")}} {{_(_="ala")}} {{_(_="sona")}} {{_(_="e")}} {{_(_="toki")}} {{_(_="mi")}}?** - Comprends-tu ce que je dis?
 - **{{_(_="sona")}}** - Oui.
 - **{{_(_="sona")}} {{_(_="ala")}}** - Non.

Note : Dans le cas de verbes composés comme avec *kama* c'est bien le
premier mot du verbe qui entre en compte.

- **{{_(_="sina")}} {{_(_="kama")}} {{_(_="ala")}} {{_(_="kama")}} {{_(_="sona")}} {{_(_="e")}} {{_(_="toki")}} {{_(_="pona")}}?** - Apprends-tu le toki pona?
 - **{{_(_="kama")}}** - Oui.
 - **{{_(_="kama")}} {{_(_="ala")}}** - Non.

## *tan* comme substantif

*tan* utilisé comme un nom signifie « origine », « cause ».

- **{{_(_="mi")}} {{_(_="sona")}} {{_(_="ala")}} {{_(_="e")}} {{_(_="tan")}}.** - Je ne connais pas la cause = Je ne sais pas
 pourquoi.
- **{{_(_="ona")}} {{_(_="li")}} {{_(_="pali")}} {{_(_="e")}} {{_(_="ni")}}. {{_(_="sina")}} {{_(_="sona")}} {{_(_="ala")}} {{_(_="sona")}} {{_(_="e")}} {{_(_="tan")}}?** - Sais-tu pourquoi
 il a fait cela?

## Exercices

### Traduisez ces phrases du français au toki pona.

Est-ce amusant? oui.<details>{{_(_="ni")}} {{_(_="li")}} {{_(_="musi")}} {{_(_="ala")}} {{_(_="musi")}}? {{_(_="musi")}}.</details>
Tu dois me dire pourquoi.<details>{{_(_="sina")}} {{_(_="wile")}} {{_(_="toki")}} {{_(_="e")}} {{_(_="tan")}} {{_(_="tawa")}} {{_(_="mi")}}.</details>
Y a-t-il un insecte à côté de moi?<details>{{_(_="pipi")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="ala")}} {{_(_="lon")}} {{_(_="poka")}} {{_(_="mi")}}?</details>
Est-elle belle?<details>{{_(_="ona")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="ala")}} {{_(_="pona")}} {{_(_="lukin")}}?</details>
Je n'arrive pas à dormir.<details>{{_(_="mi")}} {{_(_="ken")}} {{_(_="ala")}} {{_(_="lape")}}.</details>
Je ne veux pas te parler.<details>{{_(_="mi")}} {{_(_="wile")}} {{_(_="ala")}} {{_(_="toki")}} {{_(_="tawa")}} {{_(_="sina")}}.</details>
Il n'est pas allé au lac.<details>{{_(_="ona")}} {{_(_="li")}} {{_(_="tawa")}} {{_(_="ala")}} {{_(_="telo")}}.</details>

### Et maintenant essayez de traduire du toki pona en français.

sina wile ala wile pali? wile ala.<details>Veux-tu travailler? Non.</details>
jan utala li seli ala seli e tomo?<details>Le guerrier a-t-il brûlé la maison?</details>
jan lili li ken ala moku e telo nasa.<details>Les enfants ne peuvent pas boire d'alcool.</details>
sina kepeken ala kepeken e ni?<details>Est-ce que tu utilise ça?</details>
sina ken ala ken kama?<details>Peux-tu venir?</details>
