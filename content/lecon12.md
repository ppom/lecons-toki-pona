---
title: Leçon 12. Couleurs
date: 2022-11-12
in_search_index: true
extra:
  number: 12
---

# 12. Couleurs

## Vocabulaire

- **jelo** - jaune
- **kule** - couleur; peindre, colorer
- **laso** - bleu
- **loje** - rouge
- **pimeja** - noir, sombre
- **sitelen** - image; dessiner, écrire
- **walo** - blanc

## Couleurs simples et composées

L'ensemble du vocabulaire exprimant la couleur est indiqué dans la
liste ci-dessus. Pour exprimer des couleurs qui ne figurent pas dans
cette liste, on combine les couleurs :

-   **len laso** - vêtement bleu
-   **len laso jelo** - vêtement bleu-jaune = vêtement vert
-   **laso loje** - violet
-   **loje jelo** - orange
-   **loje walo** - rose
-   **walo pimeja** - gris

Remarques : On peut toujours préciser plus (*laso jelo walo* = « vert
pâle ») mais en général on évite de rentrer trop dans le détail. Par
ailleurs il n'y a pas d'ordre logique pour les couleurs. *loje laso* =
*laso loje* = « violet ».

### Couleurs et *pi*

Pour dire qu'une partie du vêtement est jaune et l'autre est bleue, on
évite de dire *len laso jelo* qui veut dire « vêtement vert ». On
emploie donc *en* :

-   **{{_(_="len")}} {{_(_="pi")}} {{_(_="laso")}} {{_(_="en")}} {{_(_="jelo")}}** - une robe bleue et jaune

Sans *pi*, la tournure est incorrecte :

-   **{{_(_="len")}} {{_(_="laso")}} {{_(_="en")}} {{_(_="jelo")}} {{_(_="li")}} {{_(_="lili")}}.** - la robe bleu et la couleur jaune
    sont petites.

### *kule*

*kule* est un nom, un verbe transitif et un adjectif.

-   **{{_(_="ni")}} {{_(_="li")}} {{_(_="kule")}} {{_(_="seme")}}?** - De quelle couleur est-ce?
-   **{{_(_="telo")}} {{_(_="li")}} {{_(_="kule")}} {{_(_="ala")}}** - L'eau est incolore.
-   **{{_(_="mi")}} {{_(_="kule")}} {{_(_="e")}} {{_(_="lipu")}}** - Je peins la feuille de papier.

## *sitelen*

*sitelen* signifie « image, dessiner, écrire ». Il est utilisé dans des
expressions comme *sitelen tawa* (« film, émission de télévision ») ou
*sitelen ma* (« atlas, carte »).

-   **{{_(_="sitelen")}} {{_(_="tawa")}} Pen Ulu {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="mi")}}.** - J'aime le film « Ben
    Hur ».
-   **{{_(_="o")}} {{_(_="pana")}} {{_(_="e")}} {{_(_="sitelen")}} {{_(_="ma")}} {{_(_="tawa")}} {{_(_="mi")}}.** - Passe-moi la carte routière.

## Exercices

### Traduisez ces phrases du français au toki pona.

Je ne vois pas le sac b<details>                  {{_(_="mi")}} {{_(_="lukin")}} {{_(_="ala")}} {{_(_="e")}} {{_(_="poki")}} {{_(_="laso")}}.</details>
Des petits hommes verts viennent du ciel.<details>{{_(_="jan")}} {{_(_="laso")}} {{_(_="jelo")}} {{_(_="lili")}} {{_(_="li")}} {{_(_="kama")}} {{_(_="tan")}} {{_(_="sewi")}}.</details>
J'aime le violet.<details>{{_(_="kule")}} {{_(_="loje")}} {{_(_="laso")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="mi")}}.</details>
Le ciel est bleu.<details>{{_(_="sewi")}} {{_(_="li")}} {{_(_="laso")}}.</details>
Regarde cet insecte rouge et noir.<details>{{_(_="o")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="pipi")}} {{_(_="ni")}} {{_(_="pi")}} {{_(_="loje")}} {{_(_="en")}} {{_(_="pimeja")}}.</details>
Quelle couleur aimes-tu?<details>{{_(_="kule")}} {{_(_="seme")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="sina")}}?</details>
De quelle couleur est-il?<details>{{_(_="ona")}} {{_(_="li")}} {{_(_="kule")}} {{_(_="seme")}}?</details>

### Et maintenant essayez de traduire du toki pona en français.

suno li jelo.<details>Le soleil est jaune.</details>
telo suli li laso.<details>La mer est bleue.</details>
mi wile moku e kili loje.<details>Je veux manger un fruit rouge</details>
ona li kule e tomo tawa.<details>il peint la voiture.</details>
