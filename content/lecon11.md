---
title: Leçon 11. Conjonctions; *kin*; température
date: 2022-11-11
in_search_index: true
extra:
  number: 11
---

# 11. Conjonctions; *kin*; température

## Vocabulaire

- **ante** - autre, différent
- **anu** - ou
- **en** - et
- **kin** - vraiment, certes, également
- **lete** - froid
- **lipu** - feuille, papier, page, etc.
- **mani** - monnaie, argent
- **pilin** - sentir, penser
- **taso** - mais, seulement

## *anu*, *en*

Ces conjonctions servent entre noms uniquement. On ne peut pas s'en
servir pour joindre des phrases entre elles.

### *anu*

*anu* exprime une alternative.

-   **{{_(_="sina")}} {{_(_="ken")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="kili")}} {{_(_="ni")}} {{_(_="anu")}} {{_(_="kili")}} {{_(_="ante")}}.** - Tu peux manger ce fruit
    ou un autre fruit.
-   **{{_(_="mi")}} {{_(_="wile")}} {{_(_="tawa")}} {{_(_="ma")}} Italija {{_(_="anu")}} {{_(_="ma")}} Epanja.** - Je veux aller en Italie
    ou en Espagne.

*anu* permet de poser des questions quand il y a un choix parmi deux
réponses.

-   **{{_(_="jan")}} Susan {{_(_="anu")}} {{_(_="jan")}} Lisa {{_(_="li")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="suwi")}}?** - Est-ce Susan ou Lisa
    qui {{_(_="a")}} mangé le bonbon?
-   **{{_(_="sina")}} {{_(_="jo")}} {{_(_="e")}} {{_(_="kili")}} {{_(_="anu")}} {{_(_="telo")}} {{_(_="nasa")}}?** - As-tu le fruit, ou bien est-ce
    le vin?
-   **{{_(_="sina")}} {{_(_="toki")}} {{_(_="tawa")}} {{_(_="mi")}} {{_(_="anu")}} {{_(_="ona")}}?** - Est-ce à moi ou à lui que tu
    parles?
-   **{{_(_="ona")}} {{_(_="anu")}} {{_(_="jan")}} {{_(_="ante")}} {{_(_="li")}} {{_(_="ike")}}?** - Est-ce lui ou quelqu'un d'autre qui
    est méchant?
-   **{{_(_="sina")}} {{_(_="toki")}} {{_(_="e")}} {{_(_="mama")}} {{_(_="anu")}} {{_(_="jan")}} {{_(_="lili")}}?** - Tu parles au sujet du père ou
    bien de l'enfant?
-   **{{_(_="sina")}} {{_(_="kama")}} {{_(_="anu")}} {{_(_="seme")}}?** - Bon alors tu viens ou quoi?

### *en*

*en* est utilisé quand la phrase a plusieurs sujets.

-   **{{_(_="mi")}} {{_(_="en")}} {{_(_="sina")}} {{_(_="li")}} {{_(_="jan")}} {{_(_="pona")}}.** - Moi et toi sommes amis.
-   **{{_(_="jan")}} {{_(_="lili")}} {{_(_="en")}} {{_(_="jan")}} {{_(_="suli")}} {{_(_="li")}} {{_(_="toki")}}.** - Les enfants et les adultes
    parlent.
-   **{{_(_="kalama")}} {{_(_="musi")}} {{_(_="en")}} {{_(_="meli")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="mi")}}.** - J'aime la musique et les
    filles.

*en* ne sert pas à joindre plusieurs compléments d'objet direct.

-   **{{_(_="mi")}} {{_(_="wile")}} {{_(_="e")}} {{_(_="moku")}} {{_(_="en")}} {{_(_="telo")}}.** - (incorrect)
-   **{{_(_="mi")}} {{_(_="wile")}} {{_(_="e")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="telo")}}.** - je veux de la nourriture et de l'eau.

De même *en* ne sert pas à joindre plusieurs verbes :

-   **{{_(_="ona")}} {{_(_="li")}} {{_(_="pali")}} {{_(_="li")}} {{_(_="lape")}}.** - Il travaille et il dort.

*en* s'utilise avec *pi* quand deux personnes possèdent quelque chose
en commun.

-   **{{_(_="tomo")}} {{_(_="pi")}} {{_(_="jan")}} Keli {{_(_="en")}} {{_(_="mije")}} {{_(_="ona")}} {{_(_="li")}} {{_(_="suli")}}.** - La maison de Keli et de
    sa femme est grande.
-   **{{_(_="jan")}} {{_(_="lili")}} {{_(_="pi")}} {{_(_="jan")}} Ken {{_(_="en")}} {{_(_="jan")}} Lisa {{_(_="li")}} {{_(_="suwi")}}.** - L'enfant de Ken et
    de Lisa est mignon.

## *taso*

La conjonction *taso* signifie « mais » et se met en début de phrase
uniquement, même si en français on regroupe parfois tout avec une
virgule.

-   **{{_(_="mi")}} {{_(_="wile")}} {{_(_="moku")}}. {{_(_="taso")}} {{_(_="mi")}} {{_(_="jo")}} {{_(_="ala")}} {{_(_="e")}} {{_(_="moku")}}.** - Je veux manger. Néanmoins
    je n'ai pas de nourriture.
-   **{{_(_="mi")}} {{_(_="wile")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="tomo")}} {{_(_="mi")}}. {{_(_="taso")}} {{_(_="mi")}} {{_(_="lon")}} {{_(_="ma")}} {{_(_="ante")}}.** - Je veux voir ma
    maison, mais je suis dans un autre pays.
-   **{{_(_="mi")}} {{_(_="pona")}}. {{_(_="taso")}} {{_(_="meli")}} {{_(_="mi")}} {{_(_="li")}} {{_(_="pakala")}}.** - Moi ça va, mais ma copine est
    blessée.

Employé comme adjectif ou comme adverbe, *taso* se met après le mot
qu'il qualifie (un peu comme *ala*).

-   **{{_(_="jan")}} Lisa {{_(_="taso")}} {{_(_="li")}} {{_(_="kama")}}.** - Seule Lisa est venue.
-   **{{_(_="mi")}} {{_(_="sona")}} {{_(_="e")}} {{_(_="ni")}} {{_(_="taso")}}.** - Je sais cela seulement.
-   **{{_(_="sina")}} {{_(_="ken")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="kili")}} {{_(_="ni")}} {{_(_="taso")}}** - Tu peux manger ce fruit-ci
    uniquement.
-   **{{_(_="sina")}} {{_(_="ken")}} {{_(_="lukin")}} {{_(_="taso")}} {{_(_="e")}} {{_(_="kili")}} {{_(_="ni")}}** - Tu peux uniquement regarder ce
    fruit.

## *kin*

*kin* veut dire « aussi », « encore », « vraiment ». Exemples :

-   **{{_(_="mi")}} {{_(_="tawa")}} {{_(_="ma")}} Elopa.** - Je suis allé en Europe.
-   **{{_(_="mi")}} {{_(_="kin")}} {{_(_="tawa")}} {{_(_="ma")}} Elopa.** - Moi aussi je suis allé en Europe.
-   **{{_(_="mi")}} {{_(_="mute")}} {{_(_="o")}} {{_(_="tawa")}}.** - Allons-y!
-   **{{_(_="mi")}} {{_(_="ken")}} {{_(_="ala")}}. {{_(_="mi")}} {{_(_="kin")}} {{_(_="moku")}}.** - Je ne peux pas. Je suis encore en
    train de manger.
-   **{{_(_="a")}}! {{_(_="sina")}} {{_(_="lukin")}} {{_(_="ala")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="ijo")}} {{_(_="nasa")}} {{_(_="ni")}}?** - Oh! As-tu vu cette
    chose étrange?
-   **{{_(_="mi")}} {{_(_="kin")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="ona")}}.** - Je la vois en effet.

## Température et *pilin*

La température s'exprime avec *seli* (« chaud ») et *lete* (« froid »).
Pour décrire la température ambiante on utilise ces formules toutes
faites :

-   **{{_(_="seli")}} {{_(_="li")}} {{_(_="lon")}}.** - Il fait chaud (littéralement : la chaleur est
    présente).
-   **{{_(_="lete")}} {{_(_="li")}} {{_(_="lon")}}.** - Il fait froid.
-   **{{_(_="seli")}} {{_(_="mute")}} {{_(_="li")}} {{_(_="lon")}}.** - Il fait très chaud.
-   **{{_(_="lete")}} {{_(_="lili")}} {{_(_="li")}} {{_(_="lon")}}.** - Il fait un peu froid. Il fait frais

En revanche pour décrire la température d'un objet, on utilise une
expression idiomatique sur le même moule que *{{_(_="pona")}} {{_(_="lukin")}}* :

-   **{{_(_="ilo")}} {{_(_="ni")}} {{_(_="li")}} {{_(_="lete")}} {{_(_="pilin")}}.** - Cet outil est froid (littéralement :
    froid au toucher).
-   **{{_(_="ni")}} {{_(_="li")}} {{_(_="lete")}} {{_(_="pilin")}} {{_(_="mute")}}.** - Ceci est très froid.

### Autre usages de *pilin*

*pilin* signifie « sentir » et est utilisé pour exprimer des sentiments
et des sensations.

-   **{{_(_="mi")}} {{_(_="pilin")}} {{_(_="pona")}}.** - Je me sens bien. Je suis heureux.
-   **{{_(_="mi")}} {{_(_="pilin")}} {{_(_="ike")}}.** - Je suis triste.
-   **{{_(_="mi")}} {{_(_="pilin")}} {{_(_="lete")}}.** - J'ai froid.
-   **{{_(_="sina")}} {{_(_="pilin")}} {{_(_="seme")}}?** - Comment te sens-tu?.

*pilin* signifie également « penser ».

-   **{{_(_="mi")}} {{_(_="pilin")}} {{_(_="e")}} {{_(_="ni")}}: {{_(_="sina")}} {{_(_="ike")}}.** - Je pense que tu es méchant.
-   **{{_(_="sina")}} {{_(_="pilin")}} {{_(_="e")}} {{_(_="seme")}}?** - Que penses-tu? A quoi penses-tu?.
-   **{{_(_="mi")}} {{_(_="pilin")}} {{_(_="e")}} {{_(_="ijo")}}.** - Je pense quelque-chose / Je pense à quelque
    chose.
-   **{{_(_="mi")}} {{_(_="pilin")}} {{_(_="e")}} {{_(_="meli")}} {{_(_="ni")}}.** - Je pense à cette fille.

## Exercices

### Traduisez ces phrases du français au toki pona.

Bon, tu veux venir, oui ou non?<details>{{_(_="sina")}} {{_(_="wile")}} {{_(_="kama")}} {{_(_="anu")}} {{_(_="seme")}}?</details>
tu veux de la nourriture ou de l'eau?<details>{{_(_="sina")}} {{_(_="wile")}} {{_(_="e")}} {{_(_="moku")}} {{_(_="anu")}} {{_(_="telo")}}?</details>
Je veux encore aller à ma maison.<details>{{_(_="mi")}} {{_(_="kin")}} {{_(_="wile")}} {{_(_="tawa")}} {{_(_="tomo")}} {{_(_="mi")}}.</details>
Le plat de légumes est très chaud.<details>{{_(_="poki")}} {{_(_="kili")}} {{_(_="li")}} {{_(_="seli")}} {{_(_="pilin")}} {{_(_="mute")}}.</details>
J'aime les monnaies étrangères.<details>{{_(_="mani")}} {{_(_="pi")}} {{_(_="ma")}} {{_(_="ante")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="mi")}}.</details>
Je veux partir, mais je ne peux pas.<details>{{_(_="mi")}} {{_(_="wile")}} {{_(_="tawa")}}. {{_(_="taso")}} {{_(_="mi")}} {{_(_="ken")}} {{_(_="ala")}}.</details>
Je suis seul.<details>{{_(_="mi")}} {{_(_="taso")}} {{_(_="li")}} {{_(_="lon")}}.</details>

### Et maintenant essayez de traduire du toki pona en français.

mi olin kin e sina.<details>Je t'aime encore.Je t'aime aussi.</details>
mi pilin e ni: ona li jo ala e mani.<details>Je pense qu'il n'a pas d'argent.</details>
mi wile lukin e ma ante.<details>Je veux voir des pays étrangers.</details>
mi wile ala e ijo. mi lukin taso.<details>Je ne veux rien; je ne fais que regarder.</details>
sina wile toki tawa mije anu meli?<details>Tu veux parler à la fille ou au garçon?</details>
