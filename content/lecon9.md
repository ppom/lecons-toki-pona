---
title: Leçon 9. Questions avec *seme*
date: 2022-11-09
in_search_index: true
extra:
  number: 9
---

# 9. Questions avec *seme*

## Vocabulaire

- **olin** - amour, affection
- **seme** - pronom interrogatif
- **sin** - nouveau, en plus
- **supa** - meuble
- **suwi** - sucré, doux, mignon

## Questions avec *seme*

À la [leçon 7](../lecon7) nous avons appris à poser des questions
simples en utilisant la formule : verbe + *ala* + verbe. Pour poser des
questions auxquelles on ne répond pas par oui ou par non, on utilise le
pronom interrogatif *seme* en lieu et place du mot sur lequel porte la
question.

*seme* s'utilise comme un sujet; comme un adjectif; comme un complément
d'objet direct...

-   **{{_(_="seme")}} {{_(_="li")}} {{_(_="utala")}} {{_(_="e")}} {{_(_="sina")}}?** - Qu'est-ce qui t'a blessé?
-   **{{_(_="seme")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="sina")}}?** - Qu'est-ce qui te plaît?
-   **{{_(_="toki")}} {{_(_="seme")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="sina")}}?** - Quelle langue aimes-tu?
-   **{{_(_="jan")}} {{_(_="seme")}} {{_(_="li")}} {{_(_="kama")}}?** - Qui vient (littéralement quelle personne)?
-   **{{_(_="jan")}} {{_(_="seme")}} {{_(_="li")}} {{_(_="tawa")}} {{_(_="poka")}} {{_(_="sina")}}?** - Qui va avec toi?
-   **{{_(_="tomo")}} {{_(_="seme")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="monsi")}} {{_(_="tomo")}} {{_(_="sina")}}?** - Quelle maison est derrière
    ta maison?
-   **{{_(_="sina")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="seme")}}?** - Que regardes-tu?

Il s'utilise également après une préposition:

-   **{{_(_="sina")}} {{_(_="pali")}} {{_(_="e")}} {{_(_="ni")}} {{_(_="tan")}} {{_(_="seme")}}?** - Pourquoi fais-tu cela?
-   **{{_(_="ona")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="seme")}}?** - Où est-il?

Si on a un doute sur l'usage de *seme*, il suffit de formuler la phrase
en remplaçant *seme* par *ni* comme si on connaissait la réponse. *seme*
s'emploie exactement de la même façon que *ni*.

## Divers mots de vocabulaire

Nous arrivons à la moitié des leçons, et déjà la plupart de la grammaire
a été vue. Il reste à finir de couvrir le vocabulaire, ce qui nous
ferons petit à petit au cours de chaque leçon.

### *supa*

Ce mot représente toute surface plane, et signifie plus généralement
tout meuble, table, siège, lit, sofa etc. sur lequel on peut poser
quelque chose.

-   **{{_(_="mi")}} {{_(_="pana")}} {{_(_="e")}} {{_(_="kili")}} {{_(_="lon")}} {{_(_="supa")}}.** - Je pose le fruit sur la table.
-   **{{_(_="o")}} {{_(_="tawa")}} {{_(_="tan")}} {{_(_="supa")}} {{_(_="lape")}} {{_(_="sina")}}!** - Sors de ton lit!
    [*{{_(_="supa")}} {{_(_="lape")}}* = lit]

### *suwi*

Ce mot provient de « sweet » en anglais, et signifie à la fois sucré et
doux.

-   **{{_(_="jan")}} {{_(_="lili")}} {{_(_="sina")}} {{_(_="li")}} {{_(_="suwi")}}.** - Ton enfant est mignon.
-   **{{_(_="telo")}} {{_(_="kili")}} {{_(_="ni")}} {{_(_="li")}} {{_(_="suwi")}}.** - le jus de fruit est sucré.
-   **{{_(_="mi")}} {{_(_="wile")}} {{_(_="e")}} {{_(_="suwi")}}!** - Je veux un bonbon!

### *sin*

Ce mot est principalement employé comme adjectif et signifie « en
plus », « encore ». Il signifie également « nouveau ».

-   **{{_(_="jan")}} {{_(_="sin")}} {{_(_="li")}} {{_(_="kama")}}.** - Des personnes arrivent en plus; Il arrive une
    personne de plus.
-   **{{_(_="mi")}} {{_(_="wile")}} {{_(_="e")}} {{_(_="suwi")}} {{_(_="sin")}}!** - Je veux encore des bonbons.
-   **{{_(_="toki")}} {{_(_="sin")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="mi")}}.** - j'aime les langues artificielles.
    [*{{_(_="toki")}} {{_(_="sin")}}* = langue nouvelle, langue artificielle]

### *olin*

Ce mot signifie « amour, affection », entre personnes uniquement (pour
dire qu'on aime les spaghetti, utiliser *{{_(_="pona")}} {{_(_="tawa")}} {{_(_="mi")}}*).

-   **{{_(_="mi")}} {{_(_="olin")}} {{_(_="e")}} {{_(_="meli")}} {{_(_="ni")}}.** - Je suis amoureux de cette fille.
-   **{{_(_="mama")}} {{_(_="li")}} {{_(_="olin")}} {{_(_="e")}} {{_(_="jan")}} {{_(_="lili")}} {{_(_="ona")}}.** - Les parents aiment leurs enfants.
-   **{{_(_="jan")}} {{_(_="sewi")}} {{_(_="li")}} {{_(_="olin")}} {{_(_="e")}} {{_(_="jan")}} {{_(_="ali")}}.** - Dieu aime toute l'humanité.
-   **{{_(_="kili")}} {{_(_="ni")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="mi")}}.** - J'aime ce fruit.

## Exercices

### Traduisez ces phrases du français au toki pona.

Que veux-tu faire?<details>{{_(_="sina")}} {{_(_="wile")}} {{_(_="pali")}} {{_(_="e")}} {{_(_="seme")}}?</details>
Qui est-ce qui t'aime?<details>{{_(_="jan")}} {{_(_="seme")}} {{_(_="li")}} {{_(_="olin")}} {{_(_="e")}} {{_(_="sina")}}?</details>
Est-ce sucré?<details>{{_(_="ni")}} {{_(_="li")}} {{_(_="suwi")}} {{_(_="ala")}} {{_(_="suwi")}}?</details>
je vais au lit.<details>{{_(_="mi")}} {{_(_="tawa")}} {{_(_="supa")}} {{_(_="lape")}}.</details>
Est-ce que quelqu'un d'autre est arrivé?<details>{{_(_="jan")}} {{_(_="sin")}} {{_(_="li")}} {{_(_="kama")}} {{_(_="ala")}} {{_(_="kama")}}?</details>
Donne-moi un bonbon!<details>{{_(_="o")}} {{_(_="pana")}} {{_(_="e")}} {{_(_="suwi")}} {{_(_="tawa")}} {{_(_="mi")}}!</details>
Qui est là?<details>{{_(_="jan")}} {{_(_="seme")}} {{_(_="li")}} {{_(_="lon")}}?</details>
Quel insecte t'a piqué?<details>{{_(_="pipi")}} {{_(_="seme")}} {{_(_="li")}} {{_(_="pakala")}} {{_(_="e")}} {{_(_="sina")}}?</details>
Tu voyages avec qui?<details>{{_(_="sina")}} {{_(_="tawa")}} {{_(_="poka")}} {{_(_="jan")}} {{_(_="seme")}}?</details>
Il aime le jus de fruit<details>                      {{_(_="telo")}} {{_(_="kili")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="ona")}}.</details>

### Et maintenant essayez de traduire du toki pona en français.

jan Ken o, mi olin e sina.<details>Ken, je t'aime.</details>
ni li jan seme?<details>Qui est-ce?</details>
sina lon seme?<details>Où es-tu?</details>
mi lon tan seme?<details>Pourquoi suis-je ici?</details>
jan seme li meli sina?<details>Qui est ta copine / ta femme?</details>
sina tawa ma tomo tan seme?<details>Pourquoi vas-tu en ville?</details>
sina wile tawa ma seme?<details>Dans quel pays veux-tu aller?</details>
