---
title: Leçon 5. Prépositions *lon*, *kepeken*, et *tawa*
date: 2022-11-05
in_search_index: true
extra:
  number: 5
---

# 5. Prépositions *lon*, *kepeken*, et *tawa*

## Vocabulaire

- **kama** - venir, advenir, être la cause de
- **lon** - être situé sur/en/dans, exister; dans, sur, en 
- **kepeken** - utiliser; en utilisant
- **pana** - donner, envoyer, émettre, libérer
- **kiwen** - pierre, caillou; dur; comme pierre
- **poki** - récipient, bol, verre, coupe, boîte, etc.
- **kon** - air, atmosphère, esprit, vent
- **toki** - langage; parler, dire
- **tawa** - aller vers, déplacer, se déplacer; vers, pour

## *lon*

*lon* peut être utilisé à la fois comme verbe et comme préposition.
Considérons ces exemples :

-   **{{_(_="mi")}} {{_(_="lon")}} {{_(_="tomo")}}.** - Je suis dans la maison.
-   **{{_(_="mi")}} {{_(_="moku")}} {{_(_="lon")}} {{_(_="tomo")}}.** - Je mange dans la maison.

Dans le premier exemple, *lon* est le verbe de la phrase. Dans le
second, c'est une préposition. Voici d'autres exemples :

-   **{{_(_="suno")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="sewi")}}.** - Le soleil est dans le ciel.
-   **{{_(_="mi")}} {{_(_="telo")}} {{_(_="e")}} {{_(_="mi")}} {{_(_="lon")}} {{_(_="tomo")}} {{_(_="telo")}}.** - Je me lave dans la salle d'eau.
    (en effet en leçon 5 nous avons vu que *tomo telo* signifie salle de
    bains, etc.)
-   **{{_(_="kili")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="poki")}}.** - Le fruit est dans la corbeille.
-   **{{_(_="pipi")}} {{_(_="ni")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="ma")}} {{_(_="telo")}}.** - Cet insecte est dans la boue.
-   **{{_(_="jan")}} {{_(_="utala")}} {{_(_="li")}} {{_(_="utala")}} {{_(_="lon")}} {{_(_="ma")}} {{_(_="tomo")}}.** - les soldats se battent en
    ville.

## Usage de *lon* avec *wile*

Puisque *lon* peut être utilisé comme préposition ou comme verbe, le
sens de la phrase peut être difficile à saisir quand on utilise une
phrase avec *wile*. Par exemple :

-   **{{_(_="mi")}} {{_(_="wile")}} {{_(_="lon")}} {{_(_="tomo")}}.** - Je veux être à la maison. *ou* Je veux
    (quelque chose) dans une maison.

Cette phrase a deux sens possibles. Pour éviter l'ambiguïté il vaut
mieux couper la phrase en deux :

-   **{{_(_="mi")}} {{_(_="wile")}} {{_(_="e")}} {{_(_="ni")}}: {{_(_="mi")}} {{_(_="lon")}} {{_(_="tomo")}}.** - Je veux ceci : je suis à la maison.

Le toki pona utilise souvent cette construction en *{{_(_="e")}} {{_(_="ni")}}:* pour traduire
les propositions subordonnées comme dans « Tu m'a dit que tu
mangeais ». Cette phrase se traduit ainsi littéralement :

-   **{{_(_="sina")}} {{_(_="toki")}} {{_(_="e")}} {{_(_="ni")}} {{_(_="tawa")}} {{_(_="mi")}}: {{_(_="sina")}} {{_(_="moku")}}.** - Tu m'a dit ceci : tu
    manges

Cet usage de « *ni:* » est très courant en toki pona, et nous
l'emploierons encore dans la suite des leçons.

## *lon* comme verbe transitif

Ce paragraphe traite d'un sens particulier du mot *lon*. Pour
comprendre le reste du paragraphe voyons le sens de deux mots (qui
serons traités plus tard) : *lape* signifie « repos » et *pini* signifie
« terminer ». Normalement pour réveiller quelqu'un on dit ceci :

-   **{{_(_="mi")}} {{_(_="pini")}} {{_(_="e")}} {{_(_="lape")}} {{_(_="sina")}}.** - J'ai mis fin à ton sommeil = je t'ai
    réveillé.

Mais on peut également dire cela avec *lon* :

-   **{{_(_="mi")}} {{_(_="lon")}} {{_(_="e")}} {{_(_="sina")}}.** - Je t'ai rendu présent (et éveillé).

Dans cette expression toute faite, l'image est que le dormeur n'est
pas réellement présent avant d'être réveillé. En revanche on ne
pourrait pas dire « *{{_(_="sina")}} {{_(_="lon")}} {{_(_="e")}} {{_(_="mi")}}* » (« tu m'a réveillé ») parce que
la personne qui dort est déjà présente dans sa propre existence en
dormant.

En fait cette discussion sur ce sens particulier de *lon* a surtout
l'intérêt de fournir à nouveau un exemple de cette façon générale qu'a
le toki pona de permettre de fabriquer automatiquement des verbes
transitifs à partir de verbe intransitifs ou d'adjectifs :

-   **{{_(_="mi")}}** (*adjectif ou verbe intransitif*) **{{_(_="e")}} {{_(_="sina")}}.** - Je t'ai
    rendu (*adjectif*)
-   **{{_(_="mi")}} {{_(_="nasa")}} {{_(_="e")}} {{_(_="sina")}}.** - Je te rends fou, je te fais tourner en
    bourrique.

## *kepeken*

D'une part, *kepeken* est un verbe transitif comme les autres :

-   **{{_(_="mi")}} {{_(_="kepeken")}} {{_(_="e")}} {{_(_="ilo")}}.** - J'utilise des outils.
-   **{{_(_="sina")}} {{_(_="wile")}} {{_(_="kepeken")}} {{_(_="e")}} {{_(_="ilo")}}.** - Tu devrais utiliser des outils.
-   **{{_(_="mi")}} {{_(_="kepeken")}} {{_(_="e")}} {{_(_="poki")}} {{_(_="ni")}}.** - J'utilise ce panier.

Mais *kepeken* est également une préposition :

-   **{{_(_="mi")}} {{_(_="moku")}} {{_(_="kepeken")}} {{_(_="ilo")}} {{_(_="moku")}}.** - Je mange en utilisant une
    fourchette/cuiller/etc.
-   **{{_(_="mi")}} {{_(_="lukin")}} {{_(_="kepeken")}} {{_(_="ilo")}} {{_(_="suno")}}.** - Je regarde en utilisant une lampe.
    ({{_(_="ilo")}} {{_(_="suno")}} = outil de lumière, lampe torche)

Note : je fais bien attention à traduire « *{{_(_="kepeken")}} {{_(_="ilo")}} {{_(_="ni")}}* » par « en
utilisant cet outil » et non « avec cet outil », bien que cette dernière
formulation soit plus naturelle en français. La raison en est que le mot
« avec » signifie également « en compagnie de » et que ce dernier sens
ne se traduit pas du tout par *kepeken* comme on verra à la leçon
suivante.

## *tawa*

### Usage de *tawa* comme verbe intransitif

Utilisé comme verbe intransitif, *tawa* s'emploie de façon similaire à
*lon*.

-   **{{_(_="mi")}} {{_(_="tawa")}} {{_(_="tomo")}} {{_(_="mi")}}.** - Je vais dans ma chambre.
-   **{{_(_="ona")}} {{_(_="mute")}} {{_(_="li")}} {{_(_="tawa")}} {{_(_="utala")}}** - Ils vont à la guerre.
-   **{{_(_="ona")}} {{_(_="li")}} {{_(_="wile")}} {{_(_="tawa")}} {{_(_="telo")}} {{_(_="suli")}}.** - Elle veut aller à la mer.
-   **{{_(_="sina")}} {{_(_="tawa")}} {{_(_="sewi")}} {{_(_="kiwen")}}.** - Tu montes en haut du rocher.

Notez que *sewi* est un nom dans cette dernière phrase. Une traduction
plus littérale serait : « Tu (sina) te-déplaces-vers (tawa) le-haut
(sewi) du-rocher (kiwen) ».

### *tawa* comme préposition

*tawa* en tant que préposition indique en général la destination.

-   **{{_(_="mi")}} {{_(_="toki")}} {{_(_="tawa")}} {{_(_="sina")}}.** - Je parle à-destination-de toi = Je te
    parle.
-   **{{_(_="ona")}} {{_(_="li")}} {{_(_="lawa")}} {{_(_="e")}} {{_(_="jan")}} {{_(_="tawa")}} {{_(_="ma")}} {{_(_="pona")}}.** - Il a mené le peuple à la bonne
    terre.
-   **{{_(_="ona")}} {{_(_="li")}} {{_(_="kama")}} {{_(_="tawa")}} {{_(_="ma")}} {{_(_="mi")}}.** - Il arrive dans mon pays.
-   **{{_(_="mi")}} {{_(_="pana")}} {{_(_="e")}} {{_(_="kili")}} {{_(_="tawa")}} {{_(_="sina")}}.** - J'émets un fruit pour toi = Je te
    donne un fruit.
-   **{{_(_="mi")}} {{_(_="pona")}} {{_(_="e")}} {{_(_="tomo")}} {{_(_="tawa")}} {{_(_="jan")}} {{_(_="pakala")}}.** - J'améliore la maison pour
    l'infirme.

*tawa* est également utilisé pour dire que l'on aime ou que l'on
n'aime pas quelque chose. On utilise pour cela une tournure bien
particulière :

-   **{{_(_="ni")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="mi")}}.** - Cela est bon pour moi = J'aime ça.
-   **{{_(_="ni")}} {{_(_="li")}} {{_(_="ike")}} {{_(_="tawa")}} {{_(_="mi")}}.** - C'est mauvais pour moi = Je n'aime pas
    ça.
-   **{{_(_="kili")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="mi")}}.** - J'aime les fruits.
-   **{{_(_="toki")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="mi")}}.** - J'aime parler / J'aime les langues.
-   **{{_(_="tomo")}} {{_(_="li")}} {{_(_="ike")}} {{_(_="tawa")}} {{_(_="mi")}}.** - Je n'aime pas les maisons.
-   **{{_(_="telo")}} {{_(_="suli")}} {{_(_="li")}} {{_(_="ike")}} {{_(_="tawa")}} {{_(_="mi")}}.** - Je n'aime pas la mer.

Souvenez vous que *e* peut être utilisé uniquement après le verbe de la
phrase; il n'y a pas de propositions subordonnées en toki pona. Par
exemple on ne peut pas traduire « j'aime regarder le paysage » par
quelque chose comme « *{{_(_="lukin")}} {{_(_="e")}} {{_(_="ma")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="mi")}}* ». Il faut couper
la phrase en deux :

-   **{{_(_="mi")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="ma")}}. {{_(_="ni")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="mi")}}.** - Je regarde le paysage;
    j'aime ça.

Bien sûr on peut aussi dire sensiblement la même chose avec les
techniques de la leçon 5 :

-   **{{_(_="ma")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="lukin")}}.** - La région est belle.

### *tawa* comme adjectif

Dans certains noms composés, *tawa* est utilisé comme adjectif
signifiant « mobile ».

-   **{{_(_="tomo")}} {{_(_="tawa")}}** - maison mobile = véhicule, voiture
-   **{{_(_="tomo")}} {{_(_="tawa")}} {{_(_="telo")}}** - bateau
-   **{{_(_="tomo")}} {{_(_="tawa")}} {{_(_="kon")}}** - avion, hélicoptère

### Ambiguité de *tawa*

À ce stade j'aimerais que vous examiniez la phrase suivante pour
essayer de trouver ses différentes significations :

**mi pana e tomo tawa sina.**

.\
.\
.\
.\
.\
.\
.\
.\
.\
.\
.\
.\

Si *tawa* est un adjectif, alors la phrase veut dire « j'ai donné ta
voiture. » Mais si c'est une préposition, alors ça change tout
puisqu'alors ça veut dire « je t'ai donné la maison. » Comme souvent,
il est difficile de distinguer.

### *tawa* comme verbe transitif

*tawa* a encore un autre usage, comme verbe transitif :

-   **{{_(_="mi")}} {{_(_="tawa")}} {{_(_="e")}} {{_(_="kiwen")}}.** - Je déplace le caillou.
-   **{{_(_="ona")}} {{_(_="li")}} {{_(_="tawa")}} {{_(_="e")}} {{_(_="len")}} {{_(_="mi")}}.** - Elle a déplacé mes vêtements.

## *kama*

Nous avons rencontré ce mot, mais il est très courant et possède des
particularités que nous allons voir maintenant.

D'abord il est utilisé avec *tawa* :

-   **{{_(_="ona")}} {{_(_="li")}} {{_(_="kama")}} {{_(_="tawa")}} {{_(_="tomo")}} {{_(_="mi")}}.** - Il est arrivé à ma maison / Il est
    venu chez moi.

Comme verbe intransitif, il signifie « se produire » :

-   **{{_(_="pakala")}} {{_(_="li")}} {{_(_="kama")}}.** - Les accidents arrivent / Un accident s'est
    produit.

Comme verbe transitif, il signifie « provoquer un évènement » :

-   **{{_(_="mi")}} {{_(_="kama")}} {{_(_="e")}} {{_(_="pakala")}}.** - J'ai causé un accident.
-   **{{_(_="sina")}} {{_(_="kama")}} {{_(_="e")}} {{_(_="ni")}}: {{_(_="mi")}} {{_(_="wile")}} {{_(_="moku")}}.** - Tu as causé cela : je veux
    manger = Tu m'a donné faim.

Notez que l'on n'aurait pas pu dire « *{{_(_="sina")}} {{_(_="kama")}} {{_(_="e")}} {{_(_="mi")}} {{_(_="wile")}} {{_(_="moku")}}*. »
Il n'y a pas de propositions subordonnées en toki pona.

Enfin, *kama* peut être utilisé avec des verbes pour indiquer une notion
de changement d'état. L'exemple typique est avec *jo*. Le verbe
composé *kama jo* signifie « prendre, obtenir » :

-   **{{_(_="mi")}} {{_(_="kama")}} {{_(_="jo")}} {{_(_="e")}} {{_(_="telo")}}.** - J'en viens à posséder l'eau = Je
    prends/obtiens l'eau.
-   **{{_(_="ona")}} {{_(_="li")}} {{_(_="kama")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="pipi")}}.** - Il aperçoit l'insecte.

Nous verrons plus tard d'autres exemples de verbes composés avec kama.

## Exercices

### Traduisez ces phrases du français au toki pona.

J'ai réparé la torche avec un petit outil.<details>{{_(_="mi")}} {{_(_="pona")}} {{_(_="e")}} {{_(_="ilo")}} {{_(_="suno")}} {{_(_="kepeken")}} {{_(_="ilo")}} {{_(_="lili")}}.</details>
J'aime le toki pona.<details>{{_(_="toki")}} {{_(_="pona")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="mi")}}.</details>
Nous leur avons donné de la nourriture.<details>{{_(_="mi")}} {{_(_="mute")}} {{_(_="li")}} {{_(_="pana")}} {{_(_="e")}} {{_(_="moku")}} {{_(_="tawa")}} {{_(_="ona")}} {{_(_="mute")}}.</details>
Ceci est pour mon ami.<details>{{_(_="ni")}} {{_(_="li")}} {{_(_="tawa")}} {{_(_="jan")}} {{_(_="pona")}} {{_(_="mi")}}.</details>
Les outils sont dans la boîte.<details>{{_(_="ilo")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="poki")}}.</details>
Cette bouteille est parmi les ordures.<details>{{_(_="poki")}} {{_(_="ni")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="jaki")}}.</details>
Je veux aller chez lui avec ma voiture.<details>{{_(_="mi")}} {{_(_="wile")}} {{_(_="tawa")}} {{_(_="tomo")}} {{_(_="ona")}} {{_(_="kepeken")}} {{_(_="tomo")}} {{_(_="tawa")}} {{_(_="mi")}}.</details>
Ils se disputent.<details>{{_(_="ona")}} {{_(_="mute")}} {{_(_="li")}} {{_(_="utala")}} {{_(_="toki")}}.</details>

### Et maintenant essayez de traduire du toki pona en français.

sina wile kama tawa tomo toki.<details>Tu devrais venir à la maison de la parole [forum IRC].</details>
jan li toki kepeken toki pona lon tomo toki.<details>On parle le toki pona sur le forum IRC.</details>
mi tawa tomo toki. ona li pona tawa mi.<details>Je vais sur le forum IRC. C'est bien pour moi. j'aime aller sur le forum IRC.</details>
sina kama jo e jan pona lon ni.<details>Tu obtiendra des amis là bas. Tu t'y feras des amis</details>

