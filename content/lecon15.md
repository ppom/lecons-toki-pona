---
title: Leçon 15. Nombres
date: 2022-11-15
in_search_index: true
extra:
  number: 15
---

# 15. Nombres

## Vocabulaire

- **luka** - cinq
- **nanpa** - nombre, numéro
- **tu** - deux; diviser, séparer
- **wan** - un; unir
- **weka** - au loin, absent; retirer, éliminer


## Avertissement

Il faut utiliser les nombres avec modération : qui se soucie de savoir
si vous avez mangé trois ou cinq prunes au repas? De toute façon le
toki pona ne permet d'exprimer que des petits nombres (disons,
inférieurs à quinze). Pour des nombres plus grands, on dira *mute*,
voire *mute mute*, toute précision supplémentaire étant le signe d'un
souci du détail superflu voire funeste (*ike* = compliqué, mal).

## Nombres cardinaux

Il y a trois mots servant à compter en toki pona : *wan* (1), *tu* (2),
et *luka* (5). On emploie le même mot *luka* pour dire « cinq » ou
« main » car il y a cinq doigts dans une main. Ces mots s'emploient
comme des adjectifs.

-   **{{_(_="tomo")}} {{_(_="tawa")}} {{_(_="wan")}}** - 1 voiture
-   **{{_(_="kili")}} {{_(_="loje")}} {{_(_="luka")}}** - cinq tomates
-   **{{_(_="oko")}} {{_(_="tu")}} {{_(_="sina")}}** - tes deux yeux

Quand on a besoin d'exprimer un nombre autre que 1, 2 ou 5, on ajoute
ces adjectifs jusqu'à ce que la somme des nombres qu'ils indiquent
fasse le nombre voulu.

**{{_(_="jan")}} {{_(_="lili")}} {{_(_="tu")}} {{_(_="wan")}}** - 2+1 = 3 enfants

**{{_(_="jan")}} {{_(_="luka")}} {{_(_="luka")}} {{_(_="tu")}}** - 12 personnes

On voit bien pourquoi il est infaisable avec ce système d'exprimer des
nombres plus grands qu'une quinzaine. Certains ont essayé de trouver
des tournures pour permettre d'exprimer des nombres de taille moyenne,
mais souvenez-vous de l'avertissement au début de cette leçon : c'est
délibérément qu'il n'y a pas de moyen d'exprimer de grands nombres.
Ce n'est donc pas la peine d'essayer, c'est *a priori* interdit :
utilisez *mute*.

## *mute*

Comme *mute* vient à la fin des qualificatifs, on peut être assez
expressif sans risquer l'ambiguïté.

-   **{{_(_="jan")}} {{_(_="mute")}}** - des personnes
-   **{{_(_="jan")}} {{_(_="lili")}} {{_(_="mute")}}** - des enfants
-   **{{_(_="jan")}} {{_(_="mute")}} {{_(_="lili")}}** - quelques personnes
-   **{{_(_="jan")}} {{_(_="mute")}} {{_(_="mute")}}** - vraiment beaucoup de personnes.
-   **{{_(_="jan")}} {{_(_="mute")}} {{_(_="suli")}}** - un très grand nombre de personnes.
-   **{{_(_="jan")}} {{_(_="mute")}} {{_(_="ike")}}** - trop de personnes.
    [*{{_(_="mute")}} {{_(_="ike")}}* = mauvaisement beaucoup = trop]
-   **{{_(_="jan")}} {{_(_="ike")}} {{_(_="mute")}}** - beaucoup de mauvaises gens.

## Nombres ordinaux

Les nombres ordinaux s'expriment avec *nanpa* suivi d'un nombre.

-   **{{_(_="jan")}} {{_(_="pi")}} {{_(_="nanpa")}} {{_(_="tu")}} {{_(_="tu")}}** - la personne numéro 4; la quatrième personne
-   **{{_(_="ni")}} {{_(_="li")}} {{_(_="jan")}} {{_(_="lili")}} {{_(_="ona")}} {{_(_="pi")}} {{_(_="nanpa")}} {{_(_="tu")}}.** - C'est son deuxième enfant.

En pratique, le bon usage veut que l'on retire le *pi* devant *nanpa*
quand il est utilisé pour exprimer un nombre ordinal, car il n'y a
jamais ambiguïté.

-   **{{_(_="meli")}} {{_(_="mi")}} {{_(_="nanpa")}} {{_(_="wan")}} {{_(_="li")}} {{_(_="nasa")}}.** - Ma première amie était folle.
-   **{{_(_="toki")}} Kanse {{_(_="li")}} {{_(_="toki")}} {{_(_="mi")}} {{_(_="nanpa")}} {{_(_="wan")}}.** - Le français est ma langue
    maternelle.

## Autres usages de *wan* et *tu*

Le verbe *wan* signifie « unir ».

-   **{{_(_="mi")}} {{_(_="en")}} {{_(_="meli")}} {{_(_="mi")}} {{_(_="li")}} {{_(_="wan")}}.** - moi et ma copine nous sommes mariés.

Comme verbe, *tu* signifie « fendre » ou « diviser ».

-   **{{_(_="o")}} {{_(_="tu")}} {{_(_="e")}} {{_(_="palisa")}} {{_(_="ni")}}.** - Casse ce baton en deux.

## Divers

Le mot du jour est *weka*. Comme verbe, il signifie « se débarasser
de », « retirer », « éloigner ».

-   **{{_(_="o")}} {{_(_="weka")}} {{_(_="e")}} {{_(_="len")}} {{_(_="sina")}}.** - Retire tes vêtements.
-   **{{_(_="o")}} {{_(_="weka")}} {{_(_="e")}} {{_(_="jan")}} {{_(_="lili")}} {{_(_="tan")}} {{_(_="ni")}}. {{_(_="ona")}} {{_(_="li")}} {{_(_="wile")}} {{_(_="ala")}} {{_(_="kute")}} {{_(_="e")}} {{_(_="ni")}}.** - Éloigne
    l'enfant d'ici. Il ne devrait pas entendre cela.

*weka* est également un adjectif et signifie « absent », « loin ».

-   **{{_(_="mi")}} {{_(_="weka")}}.** - J'étais absent.
-   **{{_(_="mi")}} {{_(_="wile")}} {{_(_="tawa")}} {{_(_="weka")}}.** - Je veux partir au loin.
-   **{{_(_="tomo")}} {{_(_="mi")}} {{_(_="li")}} {{_(_="weka")}} {{_(_="tan")}} {{_(_="tomo")}} {{_(_="sina")}}.** - Ma maison est loin de la
    tienne.
-   **{{_(_="ma")}} Mewika {{_(_="li")}} {{_(_="weka")}} {{_(_="ala")}} {{_(_="tan")}} {{_(_="ma")}} Kupa.** - Les États-unis ne sont pas
    loin de Cuba.

## Exercices

### Traduisez ces phrases du français au toki pona.

Je vois trois oiseaux.<details>{{_(_="mi")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="waso")}} {{_(_="tu")}} {{_(_="wan")}}.</details>
Beaucoup de gens viennent.<details>{{_(_="jan")}} {{_(_="mute")}} {{_(_="li")}} {{_(_="kama")}}.</details>
Le premier homme est là.<details>{{_(_="jan")}} {{_(_="pi")}} {{_(_="nanpa")}} {{_(_="wan")}} {{_(_="li")}} {{_(_="lon")}}.</details>
J'ai quatre voitures.<details>{{_(_="mi")}} {{_(_="jo")}} {{_(_="e")}} {{_(_="ilo")}} {{_(_="tawa")}} {{_(_="tu")}} {{_(_="tu")}}</details>
Il y a une dizaine de fruits dans le panier.<details>{{_(_="kili")}} {{_(_="mute")}} {{_(_="lili")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="poki")}}.</details>
J'ai retiré 25 cafards de ta salle de b<details>   {{_(_="mi")}} {{_(_="weka")}} {{_(_="e")}} {{_(_="pipi")}} {{_(_="mute")}} {{_(_="tan")}} {{_(_="tomo")}} {{_(_="telo")}} {{_(_="sina")}}.</details>

### Et maintenant essayez de traduire du toki pona en français.

mi weka e ijo tu ni.<details>Je me suis débarassé de ces deux choses.</details>
jan ni pi mani mute li jo e tomo tawa tu tu.<details>Cet homme riche a quatre voitures.</details>
