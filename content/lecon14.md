---
title: Leçon 14. Le corps
date: 2022-11-14
in_search_index: true
extra:
  number: 14
---

# 14. Le corps

## Vocabulaire

- **ko** - substance semi-solide (pâte, poudre, etc.)
- **linja** - ligne, cheveu
- **luka** - bras, main
- **lupa** - trou, orifice, porte, fenêtre
- **nena** - bosse, montagne
- **noka** - jambe, pied
- **oko** - oeil
- **palisa** - bâton, objet long et pointu
- **selo** - peau, surface externe
- **sijelo** - corps
- **sinpin** - mur, face, torse
- **uta** - bouche

## Parties du corps

Les mots suivants désignent directement une partie du corps :

-   **luka** - le bras, la main.
-   **noka** - la jambe, le pied.
-   **oko** - l'oeil.
-   **sinpin** - la face, le visage.
-   **uta** - la bouche.
-   **selo** - la peau.
-   **sijelo** - le corps.

De plus, d'autres mots désignant une partie du corps ont déjà été vus
dans les leçons précédentes :

-   **monsi** - le dos, les fesses.
-   **lawa** - la tête.
-   **poka** - la hanche.
-   **pilin** - le coeur.

Mais en général les parties du corps s'expriment avec des expressions,
en utilisant des noms génériques, que nous allons voir dans un ordre un
peu arbitraire. Une fois exposé le sens du mot générique, la
signification de l'expression est en général facile à comprendre.

### *palisa*

*palisa* désigne une chose vaguement longue, dure et pointue : bâton.

-   **{{_(_="palisa")}} {{_(_="mije")}}** - pénis
-   **{{_(_="palisa")}} {{_(_="luka")}}** - doigt
-   **{{_(_="palisa")}} {{_(_="lili")}} {{_(_="noka")}}** - orteil
-   **{{_(_="palisa")}} {{_(_="uta")}}** - langue

### *nena*

*nena* désigne une bosse, une excroissance ronde, une colline.

-   **{{_(_="nena")}} ({{_(_="lawa")}})** - nez
-   **{{_(_="nena")}} {{_(_="meli")}}** ou **{{_(_="nena")}} {{_(_="mama")}}** - sein

### *linja*

*linja* désigne quelque chose de fin et souple : fil, cheveu, etc.
Appliqué au corps, *linja* peut désigner toute sorte de pilosité donc il
vaut mieux préciser *linja lawa* si on veut vraiment parler des cheveux.

-   **{{_(_="linja")}} ({{_(_="lawa")}})** - cheveu.
-   **{{_(_="linja")}} {{_(_="lili")}} {{_(_="oko")}}** - sourcils, cils.
-   **{{_(_="linja")}} {{_(_="sinpin")}}** - barbe.
-   **{{_(_="linja")}} {{_(_="uta")}}** - moustache.

### *lupa*

*lupa* est le terme générique pour ouverture, orifice, porte, etc.

-   **{{_(_="lupa")}} {{_(_="jaki")}}** *ou* **{{_(_="lupa")}} {{_(_="monsi")}}** - anus
-   **{{_(_="lupa")}} {{_(_="kute")}}** - oreille
-   **{{_(_="lupa")}} {{_(_="meli")}}** - vagin
-   **{{_(_="lupa")}} {{_(_="nena")}}** - narine

### *Autres expressions*

Il existe plein d'autres tournures pour décrire certaines parties du
corps. En voici quelques-unes :

-   **{{_(_="anpa")}} {{_(_="lawa")}}** - cou, gorge.
-   **{{_(_="anpa")}} {{_(_="pi")}} {{_(_="sinpin")}} {{_(_="lawa")}}** - menton.
-   **{{_(_="sewi")}} {{_(_="pi")}} {{_(_="sinpin")}} {{_(_="lawa")}}** - front.
-   **{{_(_="walo")}} {{_(_="uta")}}** - dents.
-   **{{_(_="poki")}} {{_(_="kon")}}** - poumon.

## Fluides corporels

Les expressions combinent *ko* (substance ni solide ni liquide), *telo*
(« liquide ») et *pana* (« émettre »).

-   **{{_(_="telo")}} {{_(_="jelo")}}** - urine
-   **{{_(_="pana")}} {{_(_="e")}} {{_(_="telo")}} {{_(_="jelo")}}** - uriner
-   **{{_(_="telo")}} {{_(_="loje")}}** - sang
-   **{{_(_="ko")}} {{_(_="jaki")}}** - excréments
-   **{{_(_="pana")}} {{_(_="e")}} {{_(_="ko")}} {{_(_="jaki")}}** - déféquer
-   **{{_(_="pana")}} {{_(_="e")}} {{_(_="telo")}} {{_(_="oko")}}** - pleurer
-   **{{_(_="pana")}} {{_(_="e")}} {{_(_="kon")}} {{_(_="pona")}}** - rire

## Exercices

### Traduisez ces phrases du français au toki pona.

Embrasse-moi.<details>{{_(_="o")}} {{_(_="pilin")}} {{_(_="e")}} {{_(_="uta")}} {{_(_="mi")}} {{_(_="kepeken")}} {{_(_="uta")}} {{_(_="sina")}}.</details>
J'ai besoin de faire pipi.<details>{{_(_="mi")}} {{_(_="wile")}} {{_(_="pana")}} {{_(_="e")}} {{_(_="telo")}} {{_(_="jelo")}}.</details>
Mes cheveux sont mouillés<details>{{_(_="linja")}} {{_(_="mi")}} {{_(_="li")}} {{_(_="telo")}}.</details>
J'ai quelque chose dans l'oeil.<details>{{_(_="ijo")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="oko")}} {{_(_="mi")}}.</details>
Ce trou est grand.<details>{{_(_="lupa")}} {{_(_="ni")}} {{_(_="li")}} {{_(_="suli")}}.</details>
Veux-tu un chewing-gum?<details>{{_(_="sina")}} {{_(_="wile")}} {{_(_="ala")}} {{_(_="wile")}} {{_(_="e")}} {{_(_="ko")}} {{_(_="suwi")}}?</details>

### Et maintenant essayez de traduire du toki pona en français.

a! telo sijelo loje li kama tan nena kute mi!<details>Ah! Je saigne par l'oreille.</details>
selo mi li wile e ni: mi pilin e ona.<details>ma peau me démange.</details>
o pilin e nena.<details>Touche le bouton.</details>
o moli e pipi kepeken palisa.<details>Tue l'insecte avec le bâton.</details>
luka mi li jaki. mi wile telo e ona.<details>Mes mains sont sales. Je dois les laver.</details>
mi pilin e seli sijelo sina.<details>Je sens ta chaleur corporelle.</details>
