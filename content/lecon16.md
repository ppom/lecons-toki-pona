---
title: Leçon 16. *la*
date: 2022-11-16
in_search_index: true
extra:
  number: 16
---

# 16. *la*

## Vocabulaire

### Noms/verbes

- **open** - ouvrir, commencer, démarrer, allumer
- **pini** - fin; finir, arrêter, éteindre
- **sike** - cercle, cycle, boule
- **mun** - lune
- **tenpo** - temps

### Particule

- **la** - particule de contexte

## *la*

*la* est une particule utilisée pour préciser le contexte de la phrase.
Il y a trois usages assez distincts qui sont détaillés un par un.

## *la* précédé d'un seul mot

On peut modifier le sens de la phrase entière en la faisant précéder de
la construction : mot + *la*. Exemple :

-   **{{_(_="sina")}} {{_(_="wile")}} {{_(_="lape")}}.** - Tu veux dormir.
-   ***{{_(_="ken")}} {{_(_="la")}}* {{_(_="sina")}} {{_(_="wile")}} {{_(_="lape")}}.** - Tu veux *peut-être* dormir.

Le mot ainsi introduit par *la* précise le sens de toute la phrase, et
non seulement du sujet (utiliser un adjectif) ou du verbe (utiliser un
adverbe). Ici *ken la* ajoute une notion de possibilité à l'ensemble de
la phrase, et on le traduit par « peut-être ». Voici une petite liste
d'expressions avec le *la* adverbial :

-   **{{_(_="ken")}} {{_(_="la")}}** - peut-être, il se peut que
-   **{{_(_="ante")}} {{_(_="la")}}** - autrement, sinon

## Compléments de temps avec *la*

Le temps s'exprime avec une expression basée sur le nom *tenpo* :

-   **{{_(_="tenpo")}} {{_(_="suno")}}** - le jour (le temps du soleil)
-   **{{_(_="tenpo")}} {{_(_="pimeja")}}** - la nuit
-   **{{_(_="tenpo")}} {{_(_="ni")}}** - maintenant, le temps présent
-   **{{_(_="tenpo")}} {{_(_="suno")}} {{_(_="ni")}}** - ce jour, aujourd'hui
-   **{{_(_="tenpo")}} {{_(_="pimeja")}} {{_(_="ni")}}** - cette nuit
-   **{{_(_="tenpo")}} {{_(_="kama")}}** - le futur (le temps à venir)
-   **{{_(_="tenpo")}} {{_(_="kama")}} {{_(_="lili")}}** - bientôt
-   **{{_(_="tenpo")}} {{_(_="pini")}}** - le passé (le temps achevé)
-   **{{_(_="tenpo")}} {{_(_="suno")}} {{_(_="pini")}}** - hier
-   **{{_(_="tenpo")}} {{_(_="pimeja")}} {{_(_="pini")}}** - la nuit dernière
-   **{{_(_="tenpo")}} {{_(_="suno")}} {{_(_="kama")}}** - demain, le lendemain

Le complément de temps s'exprime avant la phrase, avec une expression
basée sur *tenpo* suivie de *la* :

-   **{{_(_="tenpo")}} {{_(_="pini")}} {{_(_="la")}} {{_(_="mi")}} {{_(_="weka")}}.** - Dans le passé, j'étais loin.
-   **{{_(_="tenpo")}} {{_(_="ni")}} {{_(_="la")}} {{_(_="mi")}} {{_(_="lon")}}.** - Maintenant je suis ici.
-   **{{_(_="tenpo")}} {{_(_="kama")}} {{_(_="la")}} {{_(_="mi")}} {{_(_="lape")}}.** - A l'avenir je dormirai.
-   **{{_(_="tenpo")}} {{_(_="pimeja")}} {{_(_="pini")}} {{_(_="la")}} {{_(_="mi")}} {{_(_="kama")}} {{_(_="nasa")}}.** - La nuit dernière je suis
    devenu ivre.

On utilise également *tenpo* pour indiquer un nombre d'occurrences.

-   **{{_(_="tenpo")}} {{_(_="tu")}}** - deux fois
-   **{{_(_="tenpo")}} {{_(_="mute")}}** - souvent, de nombreuses fois

Par exemple, on exprime son âge en terme de nombre de fois que l'on a
fait le tour du soleil :

-   **{{_(_="tenpo")}} {{_(_="pi")}} {{_(_="mute")}} {{_(_="seme")}} {{_(_="la")}} {{_(_="ona")}} {{_(_="li")}} {{_(_="sike")}} {{_(_="e")}} {{_(_="suno")}}?** - Combien de fois
    a-t-il fait le tour du soleil? = Quel âge a-t-il?
-   **{{_(_="tenpo")}} {{_(_="tu")}} {{_(_="tu")}} {{_(_="la")}} {{_(_="ona")}} {{_(_="li")}} {{_(_="sike")}} {{_(_="e")}} {{_(_="suno")}}.** - Il a quatre ans.

## Condition avec *la*

Quand *la* est précédé par toute une phrase, cette phrase donne le
contexte de la phrase qui suit *la*. Il s'agit d'un contexte temporel
ou conditionnel, et on traduit par : « si/quand ... alors ... ».

-   **{{_(_="mama")}} {{_(_="mi")}} {{_(_="li")}} {{_(_="moli")}} {{_(_="la")}} {{_(_="mi")}} {{_(_="pilin")}} {{_(_="ike")}}.** - Si mes parents mouraient, je
    serais triste / Quand ils sont morts, j'ai été triste / Quand ils
    mourront, je serai triste / ...

Voici d'autres exemples :

-   **{{_(_="mi")}} {{_(_="lape")}} {{_(_="la")}} {{_(_="ali")}} {{_(_="li")}} {{_(_="pona")}}.** - Quand je dors, tout va bien.
-   **{{_(_="sina")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="telo")}} {{_(_="nasa")}} {{_(_="la")}} {{_(_="sina")}} {{_(_="nasa")}}.** - Si tu bois de la bière, tu
    seras ivre.
-   **{{_(_="jan")}} {{_(_="li")}} {{_(_="moli")}} {{_(_="la")}} {{_(_="ona")}} {{_(_="li")}} {{_(_="ken")}} {{_(_="ala")}} {{_(_="toki")}}.** - Quand quelqu'un est mort,
    il ne peut pas parler.
-   **{{_(_="mi")}} {{_(_="pali")}} {{_(_="mute")}} {{_(_="la")}} {{_(_="mi")}} {{_(_="wile")}} {{_(_="lape")}}.** - Quand je travaille beaucoup je
    veux dormir.

## Comparaisons

On ne peut pas exprimer directement de comparatif (« plus grand que »)
ou de superlatif (« le plus grand ») en toki pona. En général on
reformule la phrase. Pour dire qu'une chose est meilleure que l'autre
on dira que l'une est très bonne et l'autre peu bonne :

-   **{{_(_="jan")}} Lisa {{_(_="li")}} {{_(_="pona")}} {{_(_="mute")}}. {{_(_="jan")}} Susan {{_(_="li")}} {{_(_="pona")}} {{_(_="lili")}}.** - Lisa est très
    bonne; Susan est un peu bonne = Lisa est meilleure que Susan.
-   **{{_(_="mi")}} {{_(_="suli")}} {{_(_="mute")}}. {{_(_="sina")}} {{_(_="suli")}} {{_(_="lili")}}.** - Je suis plus grand que toi.
-   **{{_(_="mi")}} {{_(_="moku")}} {{_(_="mute")}} {{_(_="sina")}} {{_(_="moku")}} {{_(_="lili")}}.** - Je mange plus que toi.

## Exercices

### Traduisez ces phrases du français au toki pona.

Susan va peut-être venir.<details>{{_(_="ken")}} {{_(_="la")}} {{_(_="jan")}} Susan {{_(_="li")}} {{_(_="kama")}}.</details>
La nuit dernière j'ai regardé la télévision.<details>{{_(_="tenpo")}} {{_(_="pimeja")}} {{_(_="pini")}} {{_(_="la")}} {{_(_="mi")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="sitelen")}} {{_(_="tawa")}}.</details>
Si l'ennemi arrive, brûlez ces papier.<details>{{_(_="jan")}} {{_(_="ike")}} {{_(_="li")}} {{_(_="kama")}} {{_(_="la")}} {{_(_="o")}} {{_(_="seli")}} {{_(_="e")}} {{_(_="lipu")}} {{_(_="ni")}}.</details>
Il est peut-être à l'école.<details>{{_(_="ken")}} {{_(_="la")}} {{_(_="ona")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="tomo")}} {{_(_="sona")}}.</details>
Il faut que je travaille demain.<details>{{_(_="tenpo")}} {{_(_="suno")}} {{_(_="kama")}} {{_(_="la")}} {{_(_="mi")}} {{_(_="wile")}} {{_(_="pali")}}.</details>
Quand il fait chaud, je transpire.<details>{{_(_="seli")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="la")}} {{_(_="mi")}} {{_(_="pana")}} {{_(_="e")}} {{_(_="telo")}} {{_(_="tan")}} {{_(_="selo")}} {{_(_="mi")}}.</details>
Ouvre la porte.<details>{{_(_="o")}} {{_(_="open")}} {{_(_="e")}} {{_(_="lupa")}}.</details>
C'est la pleine lune ce soir.<details>{{_(_="tenpo")}} {{_(_="pimeja")}} {{_(_="ni")}} {{_(_="la")}} {{_(_="mun")}} {{_(_="li")}} {{_(_="suli")}}.</details>

### Et maintenant essayez de traduire du toki pona en français.

ken la jan lili li wile moku e telo.<details>L'enfant a peut-être soif.</details>
tenpo ali la o kama sona!<details>Apprend toujours!</details>
o pana e sike tawa mi.<details>Passe-moi le ballon.</details>
sina sona e toki ni la sina sona e toki pona!<details>Si tu comprends cette parole tu connais le toki pona.</details>
