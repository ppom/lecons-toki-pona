---
title: Leçon 6. Autres prépositions
date: 2022-11-06
in_search_index: true
extra:
  number: 6
---

# 6. Autres prépositions

## Vocabulaire

### Noms, verbes, adjectifs

- **anpa** - sol; bas, profond; baisser, vaincre
- **insa** - intérieur, estomac
- **monsi** - arrière, derrière, dos
- **sewi** - haut, dessus, toit, ciel

### Prépositions (le plus souvent)

- **sama** - identique, similaire
- **poka** - côté; à côté, proche, en compagnie de
- **tan** - origine, cause, provenance; parce que, à cause de,

## *anpa*, *insa*, *monsi* et *sewi*

Bien que ces mots ne soient pas des prépositions, on peut fabriquer des
quasi-prépositions en les combinant avec une préposition comme *lon* ou
*tawa*.

- **{{_(_="ona")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="sewi")}} {{_(_="mi")}}.** - Il est dans mon dessus = Il est au dessus de moi.
- **{{_(_="pipi")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="anpa")}} {{_(_="poki")}}.** - L'insecte est sous le panier.
- **{{_(_="moku")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="insa")}} {{_(_="mi")}}.** - La nourriture est à l'intérieur de moi.
- **{{_(_="len")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="monsi")}} {{_(_="mi")}}.** - Les vêtements sont derrière moi / sur mon dos.

Ici *sewi* est bien un nom, et *mi* joue le rôle de pronom possessif.
*sewi mi* signifie « mon dessus » ou « la zone au dessus de moi ».
Puisque *sewi* est simplement un nom, il faut un verbe et une
préposition, ici *lon*. N'oubliez pas le verbe!

Par ailleurs, ces mots ont un usage comme tout autre nom/verbe/adjectif.

- **{{_(_="ona")}} {{_(_="li")}} {{_(_="anpa")}} {{_(_="e")}} {{_(_="jan")}} {{_(_="utala")}}.** - Il a vaincu le guerrier.
- **{{_(_="monsi")}} {{_(_="mi")}} {{_(_="li")}} {{_(_="pakala")}}.** - J'ai mal au dos
- **{{_(_="insa")}} {{_(_="mi")}} {{_(_="li")}} {{_(_="seli")}}.** - Mon ventre est chaud / J'ai de la fièvre.

## *sama*

*sama* est un préposition, un nom et un adjectif.

- **{{_(_="jan")}} {{_(_="ni")}} {{_(_="li")}} {{_(_="sama")}} {{_(_="mi")}}.** - Cette personne est comme moi.
- **{{_(_="sama")}} {{_(_="li")}} {{_(_="ike")}}.** - L'égalité est mauvaise.
- **{{_(_="ona")}} {{_(_="li")}} {{_(_="lukin")}} {{_(_="sama")}} {{_(_="pipi")}}.** - Il ressemble à un insecte.

## *tan*

*tan* est la dernière préposition du toki pona. Nous verrons d'autres
usages de cette préposition dans les leçons ultérieures.

- **{{_(_="ona")}} {{_(_="li")}} {{_(_="kama")}} {{_(_="tan")}} {{_(_="ma")}} {{_(_="sina")}}.** - Il arrive de ton pays.
- **{{_(_="mi")}} {{_(_="moku")}} {{_(_="tan")}} {{_(_="ni")}}: {{_(_="mi")}} {{_(_="wile")}} {{_(_="moku")}}.** - Je mange parce que ceci : je veux manger = Je mange parce que j'ai faim.

## *poka*

*poka* est une préposition, un nom et un adjectif.

### *poka* comme nom/adjectif

C'est le même usage que pour *anpa*, *insa*, *monsi* et *sewi*.

Exemples :

- **{{_(_="ona")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="poka")}} {{_(_="mi")}}.** - Il est à mon côté = Il est à côté de moi.
- **{{_(_="jan")}} {{_(_="poka")}}** - quelqu'un qui est à côté de soi = voisin
- **{{_(_="poka")}} {{_(_="telo")}}** - côté de l'eau = la berge, la plage.

### *poka* comme préposition

À la différence de *anpa* et consorts, *poka* s'emploie également
directement comme préposition. Ces deux phrases ont la même
signification :

- **{{_(_="jan")}} {{_(_="pona")}} {{_(_="mi")}} {{_(_="li")}} {{_(_="moku")}} {{_(_="poka")}} {{_(_="mi")}}.** - Mon ami mange à-côté-de moi.
- **{{_(_="jan")}} {{_(_="pona")}} {{_(_="mi")}} {{_(_="li")}} {{_(_="moku")}} {{_(_="lon")}} {{_(_="poka")}} {{_(_="mi")}}.** - Mon ami mange à mon côté.

Notez que *poka* indique une notion de participation, et peut se
traduire par « avec, en compagnie de ». Attention à ne pas confondre
*poka* et *kepeken* (« avec, au moyen de ») :

- **{{_(_="mi")}} {{_(_="wile")}} {{_(_="utala")}} {{_(_="e")}} {{_(_="jan")}} {{_(_="ike")}} {{_(_="poka")}} {{_(_="sina")}}.** - Je veux combattre l'ennemi
 avec toi.
- **{{_(_="mi")}} {{_(_="wile")}} {{_(_="utala")}} {{_(_="e")}} {{_(_="jan")}} {{_(_="ike")}} {{_(_="kepeken")}} {{_(_="kiwen")}}.** - Je veux combattre
 l'ennemi avec des cailloux.

## Exercices

### Traduisez ces phrases du français au toki pona.

Mon ami est à côté de moi.<details>{{_(_="jan")}} {{_(_="pona")}} {{_(_="mi")}} {{_(_="li")}} {{_(_="poka")}} {{_(_="mi")}}. {{_(_="jan")}} {{_(_="pona")}} {{_(_="mi")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="poka")}} {{_(_="mi")}}.</details>
Le soleil est au dessus de moi.<details>{{_(_="suno")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="sewi")}} {{_(_="mi")}}.</details>
La terre est sous moi.<details>{{_(_="ma")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="anpa")}} {{_(_="mi")}}.</details>
Les mauvaises choses sont derrière moi.<details>{{_(_="ijo")}} {{_(_="ike")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="monsi")}} {{_(_="mi")}}.</details>
Je suis bien car je suis vivant.<details>{{_(_="mi")}} {{_(_="pona")}} {{_(_="tan")}} {{_(_="ni")}}: {{_(_="mi")}} {{_(_="lon")}}.</details>
Je regarde la région avec mon ami.<details>{{_(_="mi")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="ma")}} {{_(_="poka")}} {{_(_="jan")}} {{_(_="pona")}} {{_(_="mi")}}.</details>
Les gens ressemblent à des fourmis.<details>{{_(_="jan")}} {{_(_="li")}} {{_(_="lukin")}} {{_(_="sama")}} {{_(_="pipi")}}.</details>
Cet enfant stupide lave sa chemise à l'eau sale.<details>{{_(_="jan")}} {{_(_="lili")}} {{_(_="nasa")}} {{_(_="ni")}} {{_(_="li")}} {{_(_="telo")}} {{_(_="e")}} {{_(_="len")}} {{_(_="ona")}} {{_(_="kepeken")}} {{_(_="telo")}} {{_(_="jaki")}}</details>
Je veux boire du jus de fruit avec toi.<details>{{_(_="mi")}} {{_(_="wile")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="telo")}} {{_(_="kili")}} {{_(_="poka")}} {{_(_="sina")}}.</details>
Je veux manger mon fruit avec ta fourchette.<details>{{_(_="mi")}} {{_(_="wile")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="kili")}} {{_(_="mi")}} {{_(_="kepeken")}} {{_(_="ilo")}} {{_(_="moku")}} {{_(_="sina")}}.</details>

### Et maintenant essayez de traduire du toki pona en français.

poka mi li pakala.<details>J'ai mal à la hanche.</details>
mi kepeken e poki e ilo moku.<details>J'utilise un bol et une cuiller.</details>
jan li lon insa tomo.<details>Quelqu'un est à l'intérieur de la maison.</details>
