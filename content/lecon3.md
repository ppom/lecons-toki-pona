---
title: Leçon 3. Complément d'objet direct
date: 2022-11-03
in_search_index: true
extra:
  number: 3
---

# 3. Complément d'objet direct

## Vocabulaire

### Noms

- **ilo** - outil, machine
- **kili** - fruit, légume
- **ni** - ceci, cela
- **ona** - il, elle
- **ma** - pays, région, terre
- **pipi** - insecte, araignée
- **ijo** - chose, machin

### Verbes

- **jo** - avoir; possession
- **lukin** - voir, regarder; vision, regard
- **pakala** - abimer, détruire; accident
- **unpa** - avoir un rapport sexuel avec; sexe; sexuel
- **wile** - vouloir, avoir besoin de, devoir; désir

### Particule

- **e** - introduit le complément d'objet direct

## Complément d'objet direct avec *e*

La particule *e* introduit le complément d'objet direct.

-   **{{_(_="mi")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="kili")}}.** - Je mange le fruit.
-   **{{_(_="ona")}} {{_(_="li")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="pipi")}}.** - Il regarde l'insecte.
-   **{{_(_="ona")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="e")}} {{_(_="ilo")}}.** - Il répare la machine.

Notez l'usage de *pona* comme verbe transitif dans cette dernière
phrase. La plupart des mots existent comme verbe transitif et comme
verbe intransitif, et la façon simple de distinguer entre les deux est
la présence d'un complément d'objet direct commençant par *e* :

-   **{{_(_="sina")}} {{_(_="pona")}}** - Tu es bonne.
-   **{{_(_="sina")}} {{_(_="pona")}} {{_(_="e")}} {{_(_="ijo")}}** - Tu répares quelque chose.

## Le verbe *wile*

Le verbe *wile* est une sorte de verbe modal qui peut être suivi par un
verbe à l'infinitif. *wile* signifie « vouloir » ou « devoir », donc il
faut s'aider du contexte.

-   **{{_(_="mi")}} {{_(_="wile")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="ma")}}.** - Je veux voir la région.
-   **{{_(_="mi")}} {{_(_="wile")}} {{_(_="pakala")}} {{_(_="e")}} {{_(_="sina")}}.** - Je dois te détruire.

Il faut noter que *e* introduit le complément d'objet direct du verbe à
l'infinitif, et non l'infinitif lui même :

-   **{{_(_="mi")}} {{_(_="wile")}} {{_(_="e")}} {{_(_="pakala")}} {{_(_="e")}} {{_(_="sina")}}.** - Je veux la destruction et (je veux)
    toi.

## Plusieurs compléments d'objet direct

On peut utiliser plusieurs compléments d'objet direct pour le même
verbe, chacun précédé de *e*.

-   **{{_(_="mi")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="kili")}} {{_(_="e")}} {{_(_="telo")}}.** - Je mange le fruit et l'eau
-   **{{_(_="mi")}} {{_(_="wile")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="ma")}} {{_(_="e")}} {{_(_="suno")}}** - Je veux voir la terre et le soleil

## Exercices

### Traduisez ces phrases du français au toki pona.

J'ai un outil.<details>{{_(_="mi")}} {{_(_="jo")}} {{_(_="e")}} {{_(_="ilo")}}.</details>
Elle mange des fruits.<details>{{_(_="ona")}} {{_(_="li")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="kili")}}.</details>
Quelque-chose me regarde.<details>{{_(_="ijo")}} {{_(_="li")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="mi")}}.</details>
Il veut écrabouiller l'araignée.<details>{{_(_="ona")}} {{_(_="li")}} {{_(_="wile")}} {{_(_="pakala")}} {{_(_="e")}} {{_(_="pipi")}}.</details>
L'insecte a soif.<details>{{_(_="pipi")}} {{_(_="li")}} {{_(_="wile")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="telo")}}.</details>
Tu devrais boire de l'eau.<details>{{_(_="sina")}} {{_(_="wile")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="telo")}}.</details>

### Et maintenant essayez de traduire du toki pona en français.

mi lukin e ni.<details>Je regarde cela.</details>
mi wile unpa e ona.<details>je veux faire l'amour avec lui/elle.</details>
jan li wile jo e ma.<details>les gens veulent posséder la terre.</details>
pipi li wile moku e kili e telo.<details>l'insecte veut manger le fruit et boire de l'eau.</details>
