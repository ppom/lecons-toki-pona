---
title: Leçon 2. Premières phrases
date: 2022-11-02
in_search_index: true
extra:
  number: 2
---

# 2. Premières phrases

## Vocabulaire

### Noms/pronoms

- **jan** - une personne, quelqu'un, etc.
- **mi** - je, moi, nous
- **moku** - nourriture
- **sina** - tu, toi, vous
- **suno** - soleil, lumière
- **telo** - eau, liquide

### Verbes/adjectifs

- **moku** - manger, boire
- **pona** - bon, simple; réparer
- **suli** - grand, important

### Particule

- **li** - sépare le sujet du verbe  (voir plus bas)

## Phrases avec *mi* ou *sina* comme sujet

Les phrases dont le sujet est *mi* ou *sina* sont les plus simples en
toki pona. Il suffit de mettre le sujet, puis le verbe.

-   **{{_(_="mi")}} {{_(_="moku")}}.** - je mange.

Par ailleurs en toki pona il n'y a pas de verbe « être ». Pour dire
« je suis (quelque-chose) » on donne le sujet puis directement
l'adjectif ou le nom :

-   **{{_(_="mi")}} {{_(_="pona")}}.** - je (suis) bon.
-   **{{_(_="mi")}} {{_(_="jan")}}.** - je (suis) une personne.
-   **{{_(_="sina")}} {{_(_="suli")}}.** - tu (es) grand/gros/important.

Dans la suite des leçons, tout ce qui concerne les verbes intransitifs
se rapporte également aux noms et adjectifs quand ils utilisés sous
forme de verbe « être (quelque-chose) ».

## L'ambiguïté du toki pona

### Sens multiples

Comme il y a très peu de mots en toki pona, la plupart des mots ont
plusieurs sens. Par exemple, *suli* peut signifier long, haut, large,
gros... ou important. Ces ambiguïtés sont inévitables, mais dans la
philosophie du toki pona ce n'est pas nécessairement une mauvaise
chose : à cause du caractère vague du vocabulaire, en parlant toki pona
on est obligé de s'intéresser à l'essentiel au lieu de se perdre dans
les détails.

### Singulier et pluriel

Le toki pona est ambigu d'une autre façon : il ne permet pas de dire si
un mot est singulier ou au pluriel. Par exemple, *jan* peut dire soit
« une personne » ou « les gens ». On verra en [leçon 4](../lecon4)
une façon d'indiquer explicitement le pluriel, mais cette méthode est
facultative : ce n'est pas parce qu'on n'indique pas le pluriel que
le mot n'est pas pluriel.

### Nom / verbe / adjectif

Une phrase en toki pona comporte généralement soit un verbe, soit un
adjectif ou un nom (avec le verbe « être » sous-entendu). L'ambiguïté
peut apparaître quand un même mot a plusieurs catégories grammaticales,
par exemple quand le même mot s'emploie selon les cas comme verbe, nom
ou adjectif. Ainsi, techniquement, les phrases suivantes ont plusieurs
sens possibles :

-   **{{_(_="mi")}} {{_(_="moku")}}.** - Je mange *ou* je suis de la nourriture.
-   **{{_(_="sina")}} {{_(_="pona")}}.** - Tu es bon *ou* tu répares.

Dans la pratique, le contexte doit donner le sens le plus probable. On
entend rarement quelqu'un dire qu'il est de la nourriture, donc *mi
moku* signifie sans doute « Je mange. » De même on répare rarement dans
l'absolu (sans préciser quoi), donc *sina pona* signifie sans doute
« tu es bon. »

Pour les phrases comme *sina pona*, il y a d'autres moyens d'être plus
précis, mais nous verrons cela dans la prochaine leçon.

### L'absence de conjugaison

Il n'y a ni déclinaison ni conjugaison en toki pona. On ne distingue
pas entre le présent, le passé, le futur, le conditionnel, etc.

-   **{{_(_="mi")}} {{_(_="moku")}}.** - Je mange / mangeais / mangerai / *etc.*

(On verra en [leçon 16](../lecon16) comment indiquer que l'action se passe
dans le passé, le présent, ou le futur.)

## Phrases dont le sujet est autre que *mi* ou *sina*

Quand le sujet de la phrase n'est ni *mi* ni *sina*, on intercale la
particule *li* entre sujet et le verbe.

-   **{{_(_="telo")}} {{_(_="li")}} {{_(_="pona")}}.** - L'eau est bonne.
-   **{{_(_="suno")}} {{_(_="li")}} {{_(_="suli")}}.** - Le soleil est grand.
-   **{{_(_="moku")}} {{_(_="li")}} {{_(_="pona")}}.** - La nourriture est bonne / manger est bon.

La particule *li* est utilisée uniquement quand le sujet n'est pas *mi*
ou *sina*. Dès que le sujet de la phrase devient plus compliqué qu'un
simple mot l'intérêt de *li* devient évident, comme on le verra à la
leçon suivante.

## Plusieurs verbes pour le même sujet

Quand plusieurs verbes ont le même sujet, on peut tout regrouper dans la
même phrase en ajoutant les verbes à la suite, chaque nouveau verbe
étant précédé par *li* :

-   **{{_(_="suno")}} {{_(_="li")}} {{_(_="suli")}} {{_(_="li")}} {{_(_="pona")}}.** - Le soleil est grand et est bon.
-   **{{_(_="mi")}} {{_(_="moku")}} {{_(_="li")}} {{_(_="suli")}}.** - Je mange et suis gros.

Ici *li* est toujours omis avant *moku* parce que le sujet de la phrase
est *mi* ou *sina*. En revanche *li* est présent devant le second verbe,
*suli*. Sans ce *li* la phrase serait confuse.

## Exercices

### Traduisez ces phrases du français au toki pona.

Les gens sont bons.<details>{{_(_="jan")}} {{_(_="li")}} {{_(_="pona")}}.</details>
Je mange.<details>{{_(_="mi")}} {{_(_="moku")}}.</details>
Tu es grand.<details>{{_(_="sina")}} {{_(_="suli")}}.</details>
L'eau est simple.<details>{{_(_="telo")}} {{_(_="li")}} {{_(_="pona")}}.</details>
Le lac est grand.<details>{{_(_="telo")}} {{_(_="li")}} {{_(_="suli")}}.</details>

### Et maintenant essayez de traduire du toki pona en français.

suno li suli.<details>Le soleil est grand.</details>
sina jan li suli.<details>Tu est une personne et tu es important / gros.</details>
jan li moku.<details>Quelqu'un mange / les gens mangent.</details>

