---
title: Leçon 4. Adjectifs, adverbes et noms composés
date: 2022-11-04
in_search_index: true
extra:
  number: 4
---

# 4. Adjectifs, adverbes et noms composés

## Vocabulaire

- **ike** - mal, mauvais, compliqué
- **jaki** - sale; ordure
- **lawa** - principal; tête; guider
- **len** - vêtement; vêtir
- **lili** - petit
- **mute** - beaucoup, nombreux
- **nasa** - fou, stupide, idiot, bizarre
- **seli** - chaud
- **sewi** - haut, supérieur; ciel
- **tomo** - maison, habitation, construction
- **utala** - guerre, bataille; combattre

## Qualificatifs

Comme vous le savez déjà, le toki pona a un vocabulaire très minimaliste
et très général, ce qui fait que l'on dispose de très peu de noms. Pour
préciser la signification d'un nom on ajoute directement après ce nom
le ou les qualificatifs qui peuvent être des adjectifs, mais aussi des
noms ou des verbes.

-   **{{_(_="jan")}} {{_(_="pona")}}** - bonne personne = ami
-   **{{_(_="jan")}} {{_(_="utala")}}** - personne (de) guerre = guerrier, soldat
-   **{{_(_="ilo")}} {{_(_="moku")}}** - outil (pour) manger = fourchette/cuiller/etc.

Certains noms ainsi qualifiés deviennent de véritables noms composés et
prennent un sens idiomatique. C'est le cas des trois exemples
ci-dessus : un *ilo moku* est quasiment toujours une fourchette/etc.

Plusieurs qualificatifs peuvent préciser davantage le nom qu'ils
qualifient. On ajoute les qualificatifs un par un derrière le nom.

-   **{{_(_="jan")}} {{_(_="utala")}} {{_(_="pona")}}** - bon soldat
-   **{{_(_="jan")}} {{_(_="pona")}} {{_(_="utala")}}** - ami combattant
-   **{{_(_="jan")}} {{_(_="utala")}} {{_(_="pona")}} {{_(_="mute")}}** - de nombreux bons soldats
-   **{{_(_="jan")}} {{_(_="utala")}} {{_(_="pona")}} {{_(_="ni")}}** - ce bon soldat

Voici quelques noms composés fréquents utilisant les mots que l'on a
déja appris. Essayez de retrouver leur sens littéral :

-   **{{_(_="jan")}} {{_(_="ike")}}** - ennemi
-   **{{_(_="jan")}} {{_(_="lawa")}}** - leader, chef
-   **{{_(_="jan")}} {{_(_="lili")}}** - enfant
-   **{{_(_="jan")}} {{_(_="pakala")}}** - blessé, accidenté
-   **{{_(_="jan")}} {{_(_="sewi")}}** - dieu
-   **{{_(_="jan")}} {{_(_="suli")}}** - adulte
-   **{{_(_="jan")}} {{_(_="unpa")}}** - amant(e), prostitué(e)
-   **{{_(_="ma")}} {{_(_="telo")}}** - boue, marais
-   **{{_(_="ma")}} {{_(_="tomo")}}** - ville
-   **{{_(_="telo")}} {{_(_="nasa")}}** - alcool, bière, vin
-   **{{_(_="tomo")}} {{_(_="telo")}}** - toilettes, salle de bains

On peut aussi faire des combinaisons d'adjectifs sans noms :

-   **{{_(_="ona")}} {{_(_="li")}} {{_(_="ike")}} {{_(_="lukin")}}** - il est laid
-   **{{_(_="sina")}} {{_(_="pona")}} {{_(_="lukin")}}** - tu es jolie
-   **{{_(_="unpa")}} {{_(_="lukin")}}** - sexy

## Pluriel, possession

On utilise le qualificatif *mute* pour préciser le pluriel (Mais cela ne
signifie pas que quand le pluriel n'est pas précisé il s'agit d'un
singulier).

-   **{{_(_="mi")}} {{_(_="mute")}}** - nous
-   **{{_(_="sina")}} {{_(_="mute")}}** - vous
-   **{{_(_="ona")}} {{_(_="mute")}}** - ils, elles, eux

Les pronoms employés comme qualificatifs servent de pronoms possessifs.

-   **{{_(_="tomo")}} {{_(_="mi")}}** - ma maison
-   **{{_(_="ma")}} {{_(_="sina")}}** - ton pays
-   **{{_(_="telo")}} {{_(_="ona")}}** - son eau

Les autres noms peuvent être utilisés de la même façon :

-   **{{_(_="len")}} {{_(_="jan")}}** - le vêtement de quelqu'un
-   **{{_(_="seli")}} {{_(_="suno")}}** - la chaleur du soleil

## Adverbes

L'adverbe suit directement le verbe qu'il modifie.

-   **{{_(_="mi")}} {{_(_="lawa")}} *{{_(_="pona")}}* {{_(_="e")}} {{_(_="jan")}}.** - Je conduis *bien* les hommes.
-   **{{_(_="mi")}} {{_(_="utala")}} *{{_(_="ike")}}*.** - Je combats *mal*.
-   **{{_(_="sina")}} {{_(_="lukin")}} **{{_(_="sewi")}}** {{_(_="e")}} {{_(_="suno")}}.** - Tu regardes *vers le haut* le
    soleil.
-   **{{_(_="ona")}} {{_(_="li")}} {{_(_="wile")}} *{{_(_="mute")}}* {{_(_="e")}} {{_(_="ni")}}.** - Il veut *beaucoup* cela.
-   **{{_(_="mi")}} {{_(_="mute")}} {{_(_="li")}} {{_(_="lukin")}} *{{_(_="lili")}}* {{_(_="e")}} {{_(_="ona")}}.** - Nous le voyons *peu*.

## Exercices

### Traduisez ce poème de jan Pije.

> mi jo e kili.\
> ona li pona li lili.\
> mi moku lili e kili lili.

<details>J'ai un fruit.</details>
<details>Il est bon et petit.</details>
<details>Je mange un peu du petit fruit.</details>

### Traduisez ces phrases du français au toki pona.

Le chef a bu de l'eau sale.<details>{{_(_="jan")}} {{_(_="lawa")}} {{_(_="li")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="telo")}} {{_(_="jaki")}}.</details>
J'ai besoin d'une fourchette.<details>{{_(_="mi")}} {{_(_="wile")}} {{_(_="e")}} {{_(_="ilo")}} {{_(_="moku")}}.</details>
Un ennemi les attaque.<details>{{_(_="jan")}} {{_(_="ike")}} {{_(_="li")}} {{_(_="utala")}} {{_(_="e")}} {{_(_="ona")}} {{_(_="mute")}}.</details>
Ce méchant homme a d'étrange vêtements.<details>{{_(_="jan")}} {{_(_="ike")}} {{_(_="ni")}} {{_(_="li")}} {{_(_="jo")}} {{_(_="e")}} {{_(_="len")}} {{_(_="nasa")}}.</details>
Nous avons bu beaucoup de vodka.<details>{{_(_="mi")}} {{_(_="mute")}} {{_(_="li")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="telo")}} {{_(_="nasa")}} {{_(_="mute")}}.</details>
Les enfants regardent les adultes.<details>{{_(_="jan")}} {{_(_="lili")}} {{_(_="li")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="jan")}} {{_(_="suli")}}.</details>

### Et maintenant essayez de traduire du toki pona en français.

mi lukin sewi e tomo suli.<details>Je lève les yeux vers le grand immeuble.</details>
seli suno li seli e tomo mi.<details>La chaleur du soleil réchauffe ma maison.</details>
jan lili li wile e telo kili.<details>L'enfant veut du jus de fruit.</details>
ona mute li nasa e jan suli.<details>Ils ont rendu fous les adultes.</details>

(Dans cette dernière phrase, remarquez comment *nasa*, typiquement un
adjectif, est transformé en verbe transitif.)

Courage : si vous avez suivi correctement les trois dernières leçons,
alors vous savez déjà un quart du vocabulaire de toki pona. Continuez
comme ça!

