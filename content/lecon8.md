---
title: Leçon 8. Genre; mots étrangers; ordres et interjections
date: 2022-11-08
in_search_index: true
extra:
  number: 8
---

# 8. Genre; mots étrangers; ordres et interjections

## Vocabulaire

### Noms/verbes/adjectifs

- **awen** - attendre, rester, demeurer
- **mama** - parent, père, mère
- **mije** - masculin, homme, mari, petit ami
- **meli** - féminin, femme, épouse, petite amie
- **nimi** - nom, mot

### Interjections

- **a!** - ah, heu, oh, hein
- **mu!** - miaou, meuh, ouah, (cri d'animal mignon)

### Particule

- **o** - introduit le vocatif et l'impératif

## Genre

Les mots ne s'accordent pas selon le genre en toki pona. En général le
genre est non précisé, mais on peut préciser le genre avec les adjectifs
*meli* (« féminin ») ou *mije* (« masculin »).

- **{{_(_="mama")}}** - père ou mère
- **{{_(_="mama")}} {{_(_="meli")}}** - mère
- **{{_(_="mama")}} {{_(_="mije")}}** - père
- **{{_(_="jan")}} {{_(_="lili")}} {{_(_="mije")}}** - fils
- **{{_(_="jan")}} {{_(_="utala")}} {{_(_="meli")}}** - guerrière

## Mots étrangers, noms propres

Le vocabulaire de base du toki pona est constitué des 118 mots du
dictionnaire officiel. Tous les noms de personnes, de langues, de pays,
de religions, etc. qui ne figurent pas dans ce dictionnaire sont des
noms dits non-officiels, ou étrangers (à noter qu'il existe quand même
une [liste](http://www.geocities.com/tokipona/learn/foreign.html) en
anglais de ces noms étrangers).

En toki pona, un nom étranger joue le rôle d'un adjectif. Il est donc
quasiment toujours précédé d'un nom. Exemple :

- ***{{_(_="ma")}} Italija* {{_(_="li")}} {{_(_="pona")}} {{_(_="lukin")}}.** - *L'Italie* est belle.
- **{{_(_="ona")}} {{_(_="li")}} {{_(_="kama")}} {{_(_="sona")}} {{_(_="e")}} *{{_(_="toki")}} Italija*** - il apprend *l'italien*
- ***{{_(_="ma")}} {{_(_="tomo")}} Lantan* {{_(_="li")}} {{_(_="suli")}}.** - *La ville de Londres* est grande.

Pour les personnes le nom utilisé devant le mot non-officiel est en
général le nom *jan*.

- **{{_(_="jan")}} Sameno {{_(_="li")}} {{_(_="pali")}} {{_(_="e")}} {{_(_="toki")}} Epelanto.** - Zamenhof a créé
 l'esperanto.
- **{{_(_="jan")}} Malijan {{_(_="li")}} {{_(_="wile")}} {{_(_="tawa")}} {{_(_="ma")}} Tosi.** - Marianne veut aller en
 Allemagne
 (*Tosi* est la transcription en toki pona de « Deutsche »)
- **{{_(_="jan")}} Pije {{_(_="li")}} {{_(_="pana")}} {{_(_="e")}} {{_(_="sona")}} {{_(_="tawa")}} {{_(_="mi")}}.** - Pierre m'instruit
 (*{{_(_="pana")}} {{_(_="e")}} {{_(_="sona")}}* = donner la connaissance, instruire)

Si on veut préciser, on peut utiliser d'autre nom, comme *meli*,
*mije*, etc. Par ailleurs, noter que la langue est flexible :

- **{{_(_="jan")}} Kanse** - un français
- **{{_(_="jan")}} Sonja** - la personne nommée Sonia
- **{{_(_="meli")}} Losi {{_(_="ni")}}** - cette fille russe
- **{{_(_="jan")}} Kanata {{_(_="mute")}} {{_(_="li")}} {{_(_="toki")}} {{_(_="kepeken")}} {{_(_="toki")}} Inli {{_(_="li")}} {{_(_="toki")}} {{_(_="kepeken")}} {{_(_="toki")}}
 Kanse.** - beaucoup de canadiens parlent anglais et français.

Pour se présenter, on emploie l'une des deux possibilité suivantes :

- **{{_(_="mi")}} {{_(_="jan")}} Jakopo.** - Je suis Jacob.
- **{{_(_="nimi")}} {{_(_="mi")}} {{_(_="li")}} Jakopo.** - Mon nom est Jacob.

Bien entendu personne ne va vous forcer à utiliser une forme tokiponisée
de votre nom. Vous pouvez parfaitement conserver votre nom, par exemple
entre guillemets :

- **{{_(_="nimi")}} {{_(_="mi")}} {{_(_="li")}} "Laurent".** - Je m'appelle Laurent.

## Vocatif

Pour interpeller quelqu'un, on dit le nom de la personne, suivi de *o*
(virgule), et on finit en ajoutant une phrase.

- **{{_(_="jan")}} Ken {{_(_="o")}}, {{_(_="pipi")}} {{_(_="li")}} {{_(_="lon")}} {{_(_="len")}} {{_(_="sina")}}.** - Ken, il y a un insecte sur ta
 veste
- **{{_(_="jan")}} Nina {{_(_="o")}}, {{_(_="sina")}} {{_(_="li")}} {{_(_="pona")}} {{_(_="lukin")}}.** - Nina, tu es belle.
- **{{_(_="mama")}} {{_(_="mi")}} {{_(_="o")}}, {{_(_="mi")}} {{_(_="wile")}} {{_(_="moku")}}.** - Papa, j'ai faim.
- **{{_(_="jan")}} {{_(_="lawa")}} {{_(_="o")}}, {{_(_="jan")}} {{_(_="ike")}} {{_(_="li")}} {{_(_="utala")}} {{_(_="e")}} {{_(_="mi")}} {{_(_="mute")}}!** - Chef! les méchants
 nous attaquent!

On peut faire suivre *o* de l'interjection *a* pour exprimer un fort
sentiment vis-à-vis de la personne interpellée. C'est à utiliser avec
modération pour exprimer vraiment une forte émotion.

- **{{_(_="meli")}} {{_(_="mi")}} {{_(_="o")}} {{_(_="a")}}!** - Ah, ma chérie!

## Impératif

La particule *o* sert également à exprimer un ordre qu'on donne à
quelqu'un. On l'emploie dans une phrase à la place du sujet *sina*.

- **{{_(_="o")}} {{_(_="pali")}}!** - travaille!
- **{{_(_="o")}} {{_(_="awen")}}.** - Attend.
- **{{_(_="o")}} {{_(_="lukin")}} {{_(_="e")}} {{_(_="ni")}}.** - Regarde ça.
- **{{_(_="o")}} {{_(_="tawa")}} {{_(_="ma")}} {{_(_="tomo")}} {{_(_="poka")}} {{_(_="jan")}} {{_(_="pona")}} {{_(_="sina")}}.** - Va en ville avec ton ami
- **{{_(_="o")}} {{_(_="toki")}} {{_(_="ala")}} {{_(_="li")}} {{_(_="moku")}}!** - tais-toi et mange!

Pour combiner vocatif et impératif, c'est-à-dire pour donner un ordre à
quelqu'un en le nommant, on ne met qu'un seul *o* après le sujet (au
lieu de « *sujet* **o, o** verbe »).

- **{{_(_="jan")}} Jowana {{_(_="o")}} {{_(_="tawa")}} {{_(_="tomo")}} {{_(_="sina")}}.** - Jeanne, va dans ta chambre.
- **{{_(_="jan")}} Pita {{_(_="o")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="kili")}} {{_(_="ni")}}.** - Pierre, mange ces courgettes.
- **{{_(_="mi")}} {{_(_="mute")}} {{_(_="o")}} {{_(_="tawa")}}.** - Partons.

Par extension, on rencontre cette forme pour exprimer une sorte
d'impératif à la troisième personne :

- **{{_(_="suno")}} {{_(_="o")}} {{_(_="lon")}}.** - Que la lumière soit.
- **{{_(_="ali")}} {{_(_="o")}} {{_(_="pona")}} {{_(_="tawa")}} {{_(_="sina")}}.** - Que tout aille bien pour toi.

## Interjections, salutations

En plus de *a* et *mu* qui représentent toutes sortes d'interjection et
de cri d'animaux, d'autres mots sont d'usage commun comme
interjections.

- **{{_(_="a")}}!** - Ah! Oh!
- **{{_(_="a")}} {{_(_="a")}} {{_(_="a")}}!** - (rire)
- **{{_(_="mu")}}!** - Meuh! Miaou! etc.
- **{{_(_="pona")}}!** - Super! Merci!
- **{{_(_="toki")}}!** - Salut! Bonjour
- **{{_(_="jan")}} Luwi {{_(_="o")}}, {{_(_="toki")}}!** - Bonjour, Louis
- **{{_(_="ike")}}!** - Quel malheur! Hélas!
- **{{_(_="pakala")}}!** - Merde! Zut!
- **{{_(_="jaki")}}!** - Beurk! Pouah!

Quelques formules toutes faites :

- **{{_(_="suno")}} {{_(_="pona")}}!** - Bonne journée!
- **{{_(_="lape")}} {{_(_="pona")}}!** - Dormez bien! Bonne nuit!
- **{{_(_="moku")}} {{_(_="pona")}}!** - Bon appétit!
- **{{_(_="mi")}} {{_(_="tawa")}}** - Au revoir! (dit par celui qui part)
- **{{_(_="tawa")}} {{_(_="pona")}}!** - Au revoir! (dit par celui qui reste)
- **{{_(_="kama")}} {{_(_="pona")}}!** - Bienvenue!

## Exercices

### Traduisez ces phrases du français au toki pona.

Susanne, mange ta soupe!<details>{{_(_="jan")}} Susan {{_(_="o")}} {{_(_="moku")}} {{_(_="e")}} {{_(_="moku")}} {{_(_="telo")}}!</details>
Va-t'en!<details>{{_(_="o")}} {{_(_="tawa")}}!</details>
Maman, attend!<details>{{_(_="mama")}} {{_(_="o")}} {{_(_="awen")}}! {{_(_="mama")}} {{_(_="meli")}} {{_(_="o")}} {{_(_="awen")}}!</details>
Je viens d'Europe.<details>{{_(_="mi")}} {{_(_="kama")}} {{_(_="tan")}} {{_(_="ma")}} Elopa.</details>
Ah ah ah! C'est amusant.<details>{{_(_="a")}} {{_(_="a")}} {{_(_="a")}}! {{_(_="ni")}} {{_(_="li")}} {{_(_="musi")}}.</details>
Je m'appelle Ken<details> {{_(_="nimi")}} {{_(_="mi")}} {{_(_="li")}} Ken</details>
Bonjour, Lisa<details> {{_(_="jan")}} Lisa {{_(_="o")}}, {{_(_="toki")}}!</details>
Je veux aller en Autriche<details> {{_(_="mi")}} {{_(_="wile")}} {{_(_="tawa")}} {{_(_="ma")}} Oselija</details>
Au revoir! (en s'en allant)<details>{{_(_="mi")}} {{_(_="tawa")}}!</details>

### Et maintenant essayez de traduire du toki pona en français.

mi wile kama sona e toki Inli.<details>Je veux apprendre l'anglais.</details>
jan Ana o pana e moku tawa mi.<details>Anna, donne moi la nourriture.</details>
o tawa musi poka mi!<details>Danse avec moi!</details>
jan Mose o lawa e mi mute tawa ma pona.<details>Moïse, conduit nous vers la bonne terre.</details>
wawa o poka sina.<details>Que la force soit avec toi.</details>
