/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./public/**/*.html", "./public/*.html"],
  theme: {
    extend: {
      listStyleType: {
        circle: 'circle',
      },
    },
  },
  plugins: [],
}
